<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('en', 'HomeController@english');
Route::get('check-location', 'HomeController@checkLocation');
Route::get('pengumuman-tryout', 'PengumumanController@index');


