<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'news';
    protected $fillable = [
        'id', 
        'title', 
        'content',        
        'title_eng',
        'content_eng',
        'created_t'
    ];
}
