<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HasilOnline extends Model
{
    //
    protected $table = 'hasil_online';
    protected $fillable = [ 
        'name', 
        'sekolah',
        'email',
        'nilai'        
    ];
}
