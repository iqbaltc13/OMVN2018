<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reregist extends Model
{
    protected $table = 'reregist';
    protected $fillable = [
        'id', 
        'title', 
        'content',        
        'title_eng',
        'content_eng',
        'created_t'
    ];
}
