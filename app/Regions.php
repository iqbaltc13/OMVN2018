<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Regions extends Model
{
    protected $table = 'regions';
    protected $fillable = [
        'id', 
        'name', 
        'pic',        
    ];
}
