<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\HasilOnline as Hasil;

class PengumumanController extends Controller
{
    //
    public function index(){
    	$data['result'] = Hasil::orderBy('nilai', 'desc')->limit(20)->get();
    	return view('pengumuman',$data);
    }
}
