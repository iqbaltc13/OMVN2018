<?php

namespace App\Http\Controllers;
use App\Regions;
use App\News;
use \Torann\GeoIP\Facades\GeoIP;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Redirect;

class HomeController extends Controller
{
    public function __construct(Request $request)
    {
        
    }

    public function index(Request $request)
    {
        if('Indonesia' != @GeoIP::getLocation()['country'] AND !$request->session()->has('selected-language')) {
            $request->session()->put('selected-language', 'true');
            return Redirect::to('en');
        }

        $data = array(
            'regions' => Regions::orderBy('created_at', 'asc')->take(42)->get(),
            'news' => News::orderBy('created_at', 'desc')->take(5)->get()
            );
        return view('index', $data);
    }
    public function english()
    {
        $data = array(
            'regions' => Regions::orderBy('created_at', 'asc')->take(42)->get(),
            'news' => News::orderBy('created_at', 'desc')->take(5)->get()
            );
        return view('index', $data);
    }

    public function checkLocation()
    {
        dd(GeoIP::getLocation()['country']);
    }
}
