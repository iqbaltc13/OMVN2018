/*
SQLyog Enterprise - MySQL GUI v8.05 
MySQL - 5.5.5-10.1.31-MariaDB : Database - medspin
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`medspin` /*!40100 DEFAULT CHARACTER SET latin1 */;

/*Table structure for table `regions` */

DROP TABLE IF EXISTS `regions`;

CREATE TABLE `regions` (
  `id` varchar(2) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  `name` text,
  `tempat` text,
  `pic` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_3` (`id`),
  KEY `id` (`id`),
  KEY `id_2` (`id`),
  KEY `id_4` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `regions` */

insert  into `regions`(`id`,`city`,`name`,`tempat`,`daftar`,`pic`,`created_at`,`updated_at`) 
  values 
  ('01','Surabaya','Region Surabaya','Surabaya, SMA Hangtuah 1','Ilham (081252826080)','2017-04-12 21:29:48',NULL),
  ('02','Bima','Region Bima','Bima, SMAN 1 Kota Bima','Ikhlas (085333381109)','2017-04-12 21:29:48',NULL),
  ('03','Bali','Region Bali','Bali, SMAN 3 Denpasar','Azizah (085645585661)','2017-04-12 21:29:48',NULL),
  ('04','Balikpapan','Region Balikpapan','Balikpapan, SMAN 1 Balikpapan Jl. Kapten Piere Tendean No.64, RT 30, Telaga Sari','Zanuba (081349760738)','2017-04-12 21:29:48',NULL),
  ('05','Bandung','Region Bandung','Bandung, SMAN 5 Bandung','Afif (082331154399)','2017-04-12 21:29:48',NULL),
  ('06','Banyuwangi 1','Region Banyuwangi','Banyuwangi, SMAN 1 Genteng','Navia (082230923042)','2017-04-12 21:29:48',NULL),
  ('07','Banyuwangi 2','Region Banyuwangi','Banyuwangi, SMPN 1 Giri','Rifki (085784084360)','2017-04-12 21:29:48',NULL),
  ('08','Blitar','Region Blitar','Blitar, SMAN 1 Blitar Jl. A. Yani No. 112, Sananwetan','Yayang (085733365755)','2017-04-12 21:29:48',NULL),
  ('09','Bogor','Region Bogor','Bogor, MAN 2 Bogor, Bogor','Alvi (0895368296024)','2017-04-12 21:29:48',NULL),
  ('10','Bojonegoro','Region Bojonegoro','Bojonegoro, SMPN 1 Bojonegoro','Bilqis (085733551856)','2017-04-12 21:29:48',NULL),
  ('11','Gorontalo','Region Gorontalo','Gorontalo, SMAN 3 Gorontalo','Rizki (082194099281)','2017-04-12 21:33:26',NULL),
  ('12','Jakarta','Region Jakarta','Jakarta, Departemen Matematika FMIPA Universitas Indonesia','Fakhri (087786454705)','2017-04-12 21:33:26',NULL),
  ('13','Jember','Region Jember','Jember, SMAN 1 Jember','Afina (085831381351)','2017-04-12 21:33:26',NULL),
  ('14','Jombang','Region Jombang','Jombang, SMAN 2 Jombang Jl. Dr. Wahidin Sudirohusodo No.1, Sengon','Yida (085704096711)','2017-04-12 21:33:26',NULL),
  ('15','Kediri','Region Kediri','Kediri, SMAN 7 Kediri','Rizky (082141264045)','2017-04-12 21:33:26',NULL),
  ('16','Lamongan','Region Lamongan','Lamongan, SMAN 1 Lamongan','Ananda (085735017865)','2017-04-12 21:33:26',NULL),
  ('17','Lumajang','Region Lumajang','Lumajang, SMAN 2 Lumajang','Wisang (085649045834)','2017-04-12 21:33:26',NULL),
  ('18','Madiun','Region Madiun','Madiun, MAN 2 Madiun','Fadhillah (085755115550)','2017-04-12 21:33:26',NULL),
  ('19','Makassar','Region Makassar','Makassar. Bosowa School Makassar','I’if (085745675089)','2017-04-12 21:33:26',NULL),
  ('20','Malang','Region Malang','Malang, Gedung O7 FMIPA Universitas Negeri Malang','Diky (082228776912)','2017-04-12 21:33:26',NULL),
  ('21','Mojokerto','Region Mojokerto','Mojokerto, SMAN 1 Kota Mojokerto','Nawal (085648694919)','2017-04-12 21:34:03',NULL),
  ('22','Pamekasan','Region Pamekasan','Pamekasan, SMAN 2 Pamekasan','Rani (085231641698)','2017-04-12 21:34:03',NULL),
  ('23','Pasuruan 1','Region Pasuruan','Pasuruan, SMAN 1 Bangil Jl. Bader No.3 Kalirejo, Bangil, Pasuruhan','Firda (085624523449)','2017-05-23 21:59:54',NULL),
  ('24','Ngawi','Region Ngawi','Ngawi, SMAN 1 Ngawi','Angggita (081357186431)','2017-06-04 17:36:15',NULL),
  ('25','Palangkaraya 4','Region  Palangkaraya',' Palangkaraya, SMP 4 Best Agro International','Arjun (085816340206)','2017-06-04 17:36:15',NULL),
  ('26','Samarinda','Region Samarinda','Samarinda, SMA 3 Tenggarong','Arif (082142744544)','2017-06-04 17:36:15',NULL),
  ('27','Semarang','Region Semarang','Semarang, Universitas Diponegoro','Gita (087758026193)','2017-06-04 17:36:15',NULL),
  ('28','Sidoarjo','Region Sidoarjo','Sidoarjo, SSC Untung Suropati','Annisa (085852178185)','2017-06-04 17:36:15',NULL),
  ('29','Solo','Region Solo','Solo, Universitas Sebelas Maret','Anita (085334065250)','2017-06-04 17:37:20',NULL),
  ('30','Trenggalek','Region Trenggalek','Trenggalek, SMAN 1 Trenggalek','Cynthia (085859272248)','2017-06-06 02:58:40',NULL),
  ('31','Tulungagung','Region Tulungagung','Tulungagung, MAN 1 Tulungagung','Reta (082139630361)','2017-06-30 13:02:22',NULL),
  ('32','Yogyakarta','Region Yogyakarta','Yogyakarta SMAN 3 Yogyakarta','Syafril (082231099895)','2017-07-14 19:32:53',NULL),
  -- ('33','ONLINE','International','ONLINE','Qonita (081230391660) / Devina (081252930000)','2017-07-16 14:50:13',NULL),('34','ONLINE','Provinsi Aceh','ONLINE','Qonita (081230391660) / Devina (081252930000)','2017-07-16 14:50:13',NULL),
  -- ('35','ONLINE','Provinsi Bangka Belitung','ONLINE','Qonita (081230391660) / Devina (081252930000)','2017-07-16 14:50:13',NULL),('36','ONLINE','Provinsi Banten','ONLINE','Qonita (081230391660) / Devina (081252930000)','2017-07-16 14:50:13',NULL),
  -- ('37','ONLINE','Provinsi Bengkulu','ONLINE','Qonita (081230391660) / Devina (081252930000)','2017-07-16 14:50:13',NULL),('38','ONLINE','Provinsi Gorontalo','ONLINE','Qonita (081230391660) / Devina (081252930000)','2017-07-16 14:50:13',NULL),
  -- ('39','ONLINE','Provinsi Jambi','ONLINE','Qonita (081230391660) / Devina (081252930000)','2017-07-16 14:50:13',NULL),('40','ONLINE','Provinsi Kalimantan Tengah','ONLINE','Qonita (081230391660) / Devina (081252930000)','2017-07-16 14:50:13',NULL),
  -- ('41','ONLINE','Provinsi Kalimantan Utara','ONLINE','Qonita (081230391660) / Devina (081252930000)','2017-07-16 14:50:13',NULL),('42','ONLINE','Provinsi Kepulauan Riau','ONLINE','Qonita (081230391660) / Devina (081252930000)','2017-07-16 14:50:13',NULL),
  -- ('43','ONLINE','Provinsi Lampung','ONLINE','Qonita (081230391660) / Devina (081252930000)','2017-07-16 14:50:13',NULL),
  -- ('44','ONLINE','Provinsi Maluku','ONLINE','Qonita (081230391660) / Devina (081252930000)','2017-07-16 14:50:13',NULL),
  -- ('45','ONLINE','Provinsi Maluku Utara','ONLINE','Qonita (081230391660) / Devina (081252930000)','2017-07-16 14:50:13',NULL),
  -- ('46','ONLINE','Provinsi Nusa Tenggara Timur','ONLINE','Qonita (081230391660) / Devina (081252930000)','2017-07-16 14:50:13',NULL),
  -- ('47','ONLINE','Provinsi Papua','ONLINE','Qonita (081230391660) / Devina (081252930000)','2017-07-16 14:50:13',NULL),
  -- ('48','ONLINE','Provinsi Papua Barat','ONLINE','Qonita (081230391660) / Devina (081252930000)','2017-07-16 14:50:13',NULL),
  -- ('49','ONLINE','Provinsi Sulawesi Barat','ONLINE','Qonita (081230391660) / Devina (081252930000)','2017-07-16 14:50:13',NULL),
  -- ('50','ONLINE','Provinsi Sulawesi Tengah','ONLINE','Qonita (081230391660) / Devina (081252930000)','2017-07-16 14:50:13',NULL),
  -- ('51','ONLINE','Provinsi Sulawesi Utara','ONLINE','Qonita (081230391660) / Devina (081252930000)','2017-07-16 14:50:13',NULL),
  -- ('52','ONLINE','Provinsi Sulawesi Tenggara','ONLINE','Qonita (081230391660) / Devina (081252930000)','2017-07-16 14:50:13',NULL),
  -- ('53','ONLINE','Provinsi Sumatera Barat','ONLINE','Qonita (081230391660) / Devina (081252930000)','2017-07-16 14:50:13',NULL),
  -- ('54','ONLINE','Provinsi Sumatera Utara','ONLINE','Qonita (081230391660) / Devina (081252930000)','2017-07-16 14:50:13',NULL),
  -- ('55','ONLINE','Provinsi Sumatera Selatan','ONLINE','Qonita (081230391660) / Devina (081252930000)','2017-07-16 14:50:13',NULL),
  -- ('56','ONLINE','Provinsi Bali','ONLINE','Qonita (081230391660) / Devina (081252930000)','2017-07-16 14:50:13',NULL),
  -- ('57','ONLINE','Provinsi Jakarta','ONLINE','Qonita (081230391660) / Devina (081252930000)','2017-07-16 14:55:14',NULL),
  -- ('58','ONLINE','Provinsi Jawa Barat','ONLINE','Qonita (081230391660) / Devina (081252930000)','2017-07-16 14:55:14',NULL),
  -- ('59','ONLINE','Provinsi Jawa Tengah','ONLINE','Qonita (081230391660) / Devina (081252930000)','2017-07-16 14:55:14',NULL),
  -- ('60','ONLINE','Provinsi Jawa Timur','ONLINE','Qonita (081230391660) / Devina (081252930000)','2017-07-16 14:55:14',NULL),
  -- ('61','ONLINE','Provinsi Kalimantan Barat','ONLINE','Qonita (081230391660) / Devina (081252930000)','2017-07-16 14:55:14',NULL),
  -- ('62','ONLINE','Provinsi Kalimantan Timur','ONLINE','Qonita (081230391660) / Devina (081252930000)','2017-07-16 14:55:14',NULL),
  -- ('63','ONLINE','Provinsi Kalimantan Selatan','ONLINE','Qonita (081230391660) / Devina (081252930000)','2017-07-16 14:55:14',NULL),
  -- ('64','ONLINE','Provinsi Nusa Tenggara Barat','ONLINE','Qonita (081230391660) / Devina (081252930000)','2017-07-16 14:55:14',NULL),
  -- ('65','ONLINE','Provinsi Sulawesi Selatan ','ONLINE','Qonita (081230391660) / Devina (081252930000)','2017-07-16 14:55:14',NULL),
  -- ('66','ONLINE','Provinsi Riau','ONLINE','Qonita (081230391660) / Devina (081252930000)','2017-07-16 14:55:14',NULL),
  -- ('67','ONLINE','Provinsi Yogyakarta','ONLINE','Qonita (081230391660) / Devina (081252930000)','2017-07-16 14:55:27',NULL), 
  ('68','Mataram','Region Mataram','Mataram, Sekolah Kristen Aletheia','Sopian (082340891883)','2017-04-12 21:33:26',NULL),
  ('69','Medan','Region Medan','Medan, Auditorium Universitas Sumatera Utara','Renita (083854318774)','2017-04-12 21:33:26',NULL),
  ('70','Palangkaraya 1','Region  Palangkaraya',' Palangkaraya, SMP 1 Best Agro International','Arjun (085816340206)','2017-06-04 17:36:15',NULL),
  ('71','Palangkaraya 2','Region  Palangkaraya',' Palangkaraya, SMP 9 Best Agro International','Arjun (085816340206)','2017-06-04 17:36:15',NULL),
  ('72','Palangkaraya 3','Region  Palangkaraya',' Palangkaraya, SMP 5 Best Agro International','Arjun (085816340206)','2017-06-04 17:36:15',NULL),
  ('73','Pasuruan 2','Region Pasuruan','Pasuruan, SMAN 4 Pasuruan','Bilqis (081330766239)','2017-05-23 21:59:54',NULL),
  ('74','Palembang','Region Palembang','Palembang, Paramount School Palembang','Bir Bik (081357527609)','2017-05-23 21:59:54',NULL),
  ('75','Ponorogo','Region Ponorogo','Ponorogo, SMPN 1 Ponorogo','Aprilia (085319352190)','2017-05-23 21:59:54',NULL),
  ('76','Probolinggo','Region Probolinggo','Probolinggo, MAN 2 Probolinggo','Deva (081235630486)','2017-05-23 21:59:54',NULL),
  ('77','Situbondo','Region Situbondo','Sidoarjo, SMAN 1 Situbondo','Ragil (081216688339)','2017-06-04 17:36:15',NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
