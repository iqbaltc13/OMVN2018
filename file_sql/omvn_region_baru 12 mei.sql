/*
SQLyog Enterprise - MySQL GUI v8.05 
MySQL - 5.5.5-10.1.31-MariaDB : Database - medspin
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`medspin` /*!40100 DEFAULT CHARACTER SET latin1 */;

/*Table structure for table `regions` */

DROP TABLE IF EXISTS `regions`;

CREATE TABLE `regions` (
  `id` varchar(2) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  `name` text,
  `tempat` text,
  `daftar` text,
  `pic` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_3` (`id`),
  KEY `id` (`id`),
  KEY `id_2` (`id`),
  KEY `id_4` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `regions` */

insert  into `regions`(`id`,`city`,`name`,`tempat`,`daftar`,`pic`,`created_at`,`updated_at`) values 
	('01','Surabaya','Region Surabaya','Surabaya, SMA Hangtuah 1','SMA Hangtuah 1','Ilham (081252826080)','2017-04-12 21:29:48',NULL),
	('02','Bima','Region Bima','Bima, SMAN 1 Kota Bima','SMAN 1 Kota Bima','Ikhlas (085333381109)','2017-04-12 21:29:48',NULL),
	('03','Bali','Region Bali','Bali, SMAN 3 Denpasar','SMAN 3 Denpasar','Azizah (085645585661)','2017-04-12 21:29:48',NULL),
	('04','Balikpapan','Region Balikpapan','Balikpapan, SMAN 1 Balikpapan Jl. Kapten Piere Tendean No.64, RT 30, Telaga Sari','SMAN 1 Balikpapan','Zanuba (081349760738)','2017-04-12 21:29:48',NULL),
	('05','Bandung','Region Bandung','Bandung, SMAN 5 Bandung','SMAN 5 Bandung','Afif (082331154399)','2017-04-12 21:29:48',NULL),
	('06','Banyuwangi 1','Region Banyuwangi','Banyuwangi, SMAN 1 Genteng','SMAN 1 Genteng','Navia (082230923042)','2017-04-12 21:29:48',NULL),
	('07','Banyuwangi 2','Region Banyuwangi','Banyuwangi, SMPN 1 Giri','SMPN 1 Giri','Rifki (085784084360)','2017-04-12 21:29:48',NULL),
	('08','Blitar','Region Blitar','Blitar, SMAN 1 Blitar Jl. A. Yani No. 112, Sananwetan','SMAN 1 Blitar','Yayang (085733365755)','2017-04-12 21:29:48',NULL),
	('09','Bogor','Region Bogor','Bogor, MAN 2 Bogor, Bogor','MAN 2 Bogor','Alvi (0895368296024)','2017-04-12 21:29:48',NULL),
	('10','Bojonegoro','Region Bojonegoro','Bojonegoro, SMPN 1 Bojonegoro','SMPN 1 Bojonegoro','Bilqis (085733551856)','2017-04-12 21:29:48',NULL),
	('11','Gorontalo','Region Gorontalo','Gorontalo, SMAN 3 Gorontalo','SMAN 3 Gorontalo','Rizki (082194099281)','2017-04-12 21:33:26',NULL),
	('12','Jakarta','Region Jakarta','Jakarta, Departemen Matematika FMIPA Universitas Indonesia','Hanya dibuka pendaftaran online','Fakhri (087786454705)','2017-04-12 21:33:26',NULL),
	('13','Jember','Region Jember','Jember, SMAN 1 Jember','SMAN 1 Jember','Afina (085831381351)','2017-04-12 21:33:26',NULL),
	('14','Jombang','Region Jombang','Jombang, SMAN 2 Jombang Jl. Dr. Wahidin Sudirohusodo No.1, Sengon','SMAN 2 Jombang','Yida (085704096711)','2017-04-12 21:33:26',NULL),
	('15','Kediri','Region Kediri','Kediri, SMAN 7 Kediri','SMAN 7 Kediri','Rizky (082141264045)','2017-04-12 21:33:26',NULL),
	('16','Lamongan','Region Lamongan','Lamongan, SMAN 1 Lamongan','SMAN 1 Lamongan','Ananda (085735017865)','2017-04-12 21:33:26',NULL),
	('17','Lumajang','Region Lumajang','Lumajang, SMAN 2 Lumajang','SMAN 2 Lumajang','Wisang (085649045834)','2017-04-12 21:33:26',NULL),
	('18','Madiun','Region Madiun','Madiun, MAN 2 Madiun','Primagama Jalan Thamrin','Fadhillah (085755115550)','2017-04-12 21:33:26',NULL),
	('19','Makassar','Region Makassar','Makassar. Bosowa School Makassar','Bosowa School Makassar','I’if (085745675089)','2017-04-12 21:33:26',NULL),
	('20','Malang','Region Malang','Malang, Gedung O7 FMIPA Universitas Negeri Malang','Gedung SPA O8 lantai 1 Sekretariat HMJ Matematika FMIPA UM','Diky (082228776912)','2017-04-12 21:33:26',NULL),
	('21','Mojokerto','Region Mojokerto','Mojokerto, SMAN 1 Kota Mojokerto','SMAN 1 Kota Mojokerto','Nawal (085648694919)','2017-04-12 21:34:03',NULL),
	('22','Pamekasan','Region Pamekasan','Pamekasan, SMAN 2 Pamekasan','SMAN 2 Pamekasan','Rani (085231641698)','2017-04-12 21:34:03',NULL),
	('23','Pasuruan 1','Region Pasuruan','Pasuruan, SMAN 1 Bangil Jl. Bader No.3 Kalirejo, Bangil, Pasuruan',NULL,'Firda (085624523449)','2017-05-23 21:59:54',NULL),
	('24','Ngawi','Region Ngawi','Ngawi, SMAN 1 Ngawi',NULL,'Angggita (081357186431)','2017-06-04 17:36:15',NULL),
	('25','Palangkaraya 4','Region  Palangkaraya',' Palangkaraya, SMP 4 Best Agro International',NULL,'Arjun (085816340206)','2017-06-04 17:36:15',NULL),
	('26','Samarinda','Region Samarinda','Samarinda, SMA 3 Tenggarong',NULL,'Arif (082142744544)','2017-06-04 17:36:15',NULL),
	('27','Semarang','Region Semarang','Semarang, Universitas Diponegoro',NULL,'Gita (087758026193)','2017-06-04 17:36:15',NULL),
	('28','Sidoarjo','Region Sidoarjo','Sidoarjo, SSC Untung Suropati',NULL,'Annisa (085852178185)','2017-06-04 17:36:15',NULL),
	('29','Solo','Region Solo','Solo, Universitas Sebelas Maret',NULL,'Anita (085334065250)','2017-06-04 17:37:20',NULL),
	('30','Trenggalek','Region Trenggalek','Trenggalek, SMAN 1 Trenggalek',NULL,'Cynthia (085859272248)','2017-06-06 02:58:40',NULL),
	('31','Tulungagung','Region Tulungagung','Tulungagung, MAN 1 Tulungagung',NULL,'Reta (082139630361)','2017-06-30 13:02:22',NULL),
	('32','Yogyakarta','Region Yogyakarta','Yogyakarta SMAN 3 Yogyakarta',NULL,'Syafril (082231099895)','2017-07-14 19:32:53',NULL),
	('68','Mataram','Region Mataram','Mataram, Sekolah Kristen Aletheia',NULL,'Sopian (082340891883)','2017-04-12 21:33:26',NULL),
	('69','Medan','Region Medan','Medan, Auditorium Universitas Sumatera Utara',NULL,'Renita (083854318774)','2017-04-12 21:33:26',NULL),
	('70','Palangkaraya 1','Region  Palangkaraya',' Palangkaraya, SMP 1 Best Agro International',NULL,'Arjun (085816340206)','2017-06-04 17:36:15',NULL),
	('71','Palangkaraya 2','Region  Palangkaraya',' Palangkaraya, SMP 9 Best Agro International',NULL,'Arjun (085816340206)','2017-06-04 17:36:15',NULL),
	('72','Palangkaraya 3','Region  Palangkaraya',' Palangkaraya, SMP 5 Best Agro International',NULL,'Arjun (085816340206)','2017-06-04 17:36:15',NULL),
	('73','Pasuruan 2','Region Pasuruan','Pasuruan, SMAN 4 Pasuruan',NULL,'Bilqis (081330766239)','2017-05-23 21:59:54',NULL),
	('74','Palembang','Region Palembang','Palembang, Paramount School Palembang',NULL,'Bir Bik (081357527609)','2017-05-23 21:59:54',NULL),
	('75','Ponorogo','Region Ponorogo','Ponorogo, SMPN 1 Ponorogo',NULL,'Aprilia (085319352190)','2017-05-23 21:59:54',NULL),
	('76','Probolinggo','Region Probolinggo','Probolinggo, MAN 2 Probolinggo',NULL,'Deva (081235630486)','2017-05-23 21:59:54',NULL),
	('77','Situbondo','Region Situbondo','Sidoarjo, SMAN 1 Situbondo',NULL,'Ragil (081216688339)','2017-06-04 17:36:15',NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
