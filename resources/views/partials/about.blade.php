<section id="about">
    <div id="aboutCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
        <li data-target="#aboutCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#aboutCarousel" data-slide-to="1"></li>
        <li data-target="#aboutCarousel" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
          <!--   <div class="item active">
                <div class="item active" style="background: blue">
                </div>
                <div class="carousel-caption" style="top: 0; left: 5%; right: 5%">
                    <div class="col-md-5" style="margin-top: 10%">
                        <img src="img/fk-bg1.JPEG" class="img-responsive">
                    </div>
                    <div class="col-md-7">
                        <h2 data-wow-duration="700ms" data-wow-delay="500ms" class="wow bounceInDown animated"><span style="color:#c4c635;">ABOUT </span>FK UNAIR</h2>
                        <p data-wow-duration="1000ms" class="wow slideInRight animated" style="text-align: justify; color: black; text-shadow: none;">
                        Fakultas Kedokteran Universitas Airlangga merupakan salah satu fakultas kedokteran tertua yang ada di Indonesia. Bangunan ini sudah ada jauh bahkan sebelum kemerdekaan indonesia, dulunya fakultas ini merupakan sekolah Netherlands Indische Artsen School (NIAS) yang mana merupakan milik dari pemerintahan Hindia Belanda hingga pada 10 Novemver 1954 telah resmi fakultas ini menjadi milik pemerintah indonesia setelah diresmikannya pendirian Universitas Airlangga.
                        <br>
                        <br>
                        Fakultas Kedokteran Universitas Airlangga terlahir dengan penuh tantangan. Berlokasi di Surabaya yang mana merupakan kota pahlawan, fakultas ini telah melahirkan banyak tokoh terkemuka di Indonesia seperti dr. Soetomo. Seakan tak pernah puas atas pencapaian yang ada, Fakultas Kedokteran Universitas Airlangga selalu berusaha untuk menjadi yang terbaik. Melalui motto "Excellence with Morality", fakultas ini selalu mencoba berkembang untuk bisa disegani di Indonesia maupun dunia.
                        Berbagai pendidikan dan penelitian telah dilakukan di fakultas ini. Fakultas yang bertempat di Jl. Prof. Dr. Moestopo No. 47 ini selalu melahirkan karya-karya hebat dari tangan dingin dan pemikirian-pemikiran jenius akademisi.
                        <br>
                        <br>
                        Kerjasama baik nasional hingga Internasional pun terus digalakkan untuk bisa menjadi lebih baik, studi dan exchange pun semakin digalakkan untuk meningkatkan kompetensi civitas akademika yang ada. Semua pihak turut berusaha untuk mewujudkan cita-cita Universitas Airlangga untuk menjadi World Class University.
                        </p>
                    </div>
                </div>
            </div> -->

            <div class="item active" style="background-image: url(assets/images/OMVN-4.JPG); background-position: center;">
                <div class="item" style="opacity: 0.5; background-color: black;">
                </div>
                <div class="carousel-caption">
                    <h2 data-wow-duration="700ms" data-wow-delay="500ms" class="wow bounceInDown animated"><span style="color:#c4c635;">OMVN </span>2018</h2>
                    <h3 data-wow-duration="1000ms" class="wow slideInLeft animated"><span>The Glory Of Numbers!</span></h3>
                </div>
            </div>
            
            <div class="item" style="background-image: url(assets/images/OMVN-3.JPG); background-position: center;">
                <div class="item" style="opacity: 0.5; background-color: black;">
                </div>
                <div class="carousel-caption">
                    <h2 data-wow-duration="700ms" data-wow-delay="500ms" class="wow bounceInDown animated"><span style="color:#c4c635;">OMVN </span>2018</h2>
                    <h3 data-wow-duration="1000ms" class="wow slideInLeft animated"><span>The Glory Of Numbers!</span></h3>
                </div>
            </div>
        </div>

        <!-- Left and right controls -->
        <a class="left carousel-control" href="#aboutCarousel" data-slide="prev" style="background: none; width: 9%">
            <i style="margin-top: 350px;" class="fa fa-chevron-left" aria-hidden="true"></i>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#aboutCarousel" data-slide="next" style="background: none; width: 9%">
            <i style="margin-top: 350px;" class="fa fa-chevron-right" aria-hidden="true"></i>
            <span class="sr-only">Next</span>
        </a>
    </div>
</section>	