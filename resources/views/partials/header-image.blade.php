<!--<section id="slider">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
    
        <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
        </ol>
        
        <div class="carousel-inner" role="listbox">            
            <div class="item active" style="background-image: url(img/fk-bg2.JPEG); background-position: center;">
                <div class="item active" style="opacity: 0.5; background-color: black;">
                </div>
                <div class="carousel-caption">
                    <h2 data-wow-duration="700ms" data-wow-delay="500ms" class="wow bounceInDown animated"><span style="color:#c4c635;">MEDSPIN </span>2017</h2>
                    <h3 data-wow-duration="1000ms" class="wow slideInLeft animated"><span>The Glory Of Numbers</span></h3>
                </div>
            </div>
        
        </div>
        
    </div>
</section>	-->
<section id="slider">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="item active" style="background-image: url(assets/images/OMVN-1.JPG); background-position: center;">
                <div class="item active" style="opacity: 0.5; background-color: black;">
                </div>
                <div class="carousel-caption">
                    <h2 data-wow-duration="700ms" data-wow-delay="500ms" class="wow bounceInDown animated"><span style="color:#c4c635;">OMVN </span>2018</h2>
                    <h3 data-wow-duration="1000ms" class="wow slideInLeft animated"><span>The Glory Of Numbers</span></h3>
                </div>
            </div>

            <div class="item" style="background-image: url(assets/images/OMVN-2.JPG); background-position: center;">
                <div class="item" style="opacity: 0.5; background-color: black;">
                </div>
                <div class="carousel-caption">
                    <h2 data-wow-duration="700ms" data-wow-delay="500ms" class="wow bounceInDown animated"><span style="color:#c4c635;">OMVN  </span>2018</h2>
                    <h3 data-wow-duration="1000ms" class="wow slideInLeft animated"><span>The Glory Of Numbers</span></h3>
                </div>
            </div>
            
            <div class="item" style="background-image: url(assets/images/OMVN-3.JPG); background-position: center;">
                <div class="item" style="opacity: 0.5; background-color: black;">
                </div>
                <div class="carousel-caption">
                    <h2 data-wow-duration="700ms" data-wow-delay="500ms" class="wow bounceInDown animated"><span style="color:#c4c635;">OMVN  </span>2018</h2>
                    <h3 data-wow-duration="1000ms" class="wow slideInLeft animated"><span>The Glory Of Numbers</span></h3>
                </div>
            </div>
            <div class="item" style="background-image: url(assets/images/OMVN-4.JPG); background-position: center;">
                <div class="item" style="opacity: 0.5; background-color: black;">
                </div>
                <div class="carousel-caption">
                    <h2 data-wow-duration="700ms" data-wow-delay="500ms" class="wow bounceInDown animated"><span style="color:#c4c635;">OMVN  </span>2018</h2>
                    <h3 data-wow-duration="1000ms" class="wow slideInLeft animated"><span>The Glory Of Numbers</span></h3>
                </div>
            </div>
        </div>

        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <i style="margin-top: 350px;" class="fa fa-chevron-left" aria-hidden="true"></i>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <i style="margin-top: 350px;" class="fa fa-chevron-right" aria-hidden="true"></i>
            <span class="sr-only">Next</span>
        </a>
    </div>
</section>	