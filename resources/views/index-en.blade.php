<!DOCTYPE html>
<html lang="id">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>MEDSPIN 2017 - Be a Medical Hero!</title>		
        <meta name="description" content="Medical Science and Application (MEDSPIN) 2017 adalah salah satu lomba terbesar yang diadakan oleh Fakultas Kedokteran Universitas Airlangga. Lomba ini akan diadakan bagi para pelajar tingkat SMA / sederajat. Lomba ini bersifat invitasi dan terbuka bagi seluruh pelajar SMA/sederajat dari semua kelas (X, XI, dan XII) baik SMA negeri maupun swasta nasional maupun internasional.">
        <meta name="keywords" content="lomba, medspin, kimia, fisika, biologi, matematika, unair, universitas airlangga, kompetisi, kedokteran">
        <meta name="author" content="Fakultas Kedokteran Universitas Airlangga">
        <meta name="viewport" content="width=device-width, initial-scale=1">

		<link rel="icon" type="image/png" href="img/logo.png">
				
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/jquery.fancybox.css">
        <link rel="stylesheet" href="css/animate.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/media-queries.css">
		<link rel="stylesheet" href="css/style-timeline.css">
        <link rel="stylesheet" href="css/style-list.css">
        <link rel="stylesheet" href="css/jQuery.verticalCarousel.css">		
		<link rel="stylesheet" href="css/flipclock.css">
		<link rel="stylesheet" href="css/font-awesome.css">		
        <script src="js/modernizr-2.6.2.min.js"></script>
    </head>
	
    <body id="body">
		<div id="preloader">
			<img src="img/preloader.gif" alt="Preloader" class="heavy-rotation">
		</div>
	
        <header id="navigation" class="navbar-fixed-top navbar">
            <div class="container">			
                <div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <i class="fa fa-bars fa-2x"></i>
                    </button>
					
                    <a class="navbar-brand" href="#" style="margin-left:10px">
						<h1 id="logo">
							<img src="img/logo.png" alt="Brandi" style="height: 45px; margin-top: 3px;">
						</h1>
					</a>
                </div>								

                <nav class="collapse navbar-collapse navbar-right" role="navigation">
                    <ul id="nav" class="nav navbar-nav">
                        <li><a href="#tentang-medspin">What is MEDSPIN?</a></li>
                        <li><a href="#jadwal-perlombaan">Schedule</a></li>
                        <li><a href="#registrasi">Registration</a></li>
                        <li><a href="#region-penyisihan">Panwil</a></li>
                        <li><a href="#galeri">Gallery</a></li>
						<li><a href="#tanya-medspin">Ask Medspin</a></li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Syllabus and Example Question<span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li>
									<a href="https://drive.google.com/drive/folders/0B8g-yuRVnzjnZWpybnY5ejJkTzg?usp=sharing" style="text-color: black;" target="__blank">Book Reference and Syllabus</a>
								</li>
								<li>
									<a href="https://drive.google.com/drive/folders/0B8g-yuRVnzjnODFocGRPTW9WY28?usp=sharing" style="text-color: black;" target="__blank">Example Question</a>									
								</li>
							</ul>
						</li>						
                    </ul>
					<a href="{{url('/')}}"><img src="img/flag-indo.png" alt="indo-lang" style="height: 20px; margin-top: 15px;"></a>							
					<a href="{{url('/en')}}"><img src="img/flag-uk.png" alt="uk-lang" style="height: 20px; margin-top: 15px;"></a>								
                </nav>				
				
            </div>
        </header>
       
		@include('partials.header-image')

		<section id="news-countdown" class="features">
			<div class="container">
				<div class="row">
					<!--<div class="col-md-7">
						<div  style="text-align : center" class="hidden-xs timeline">
							<div class="clock" style="margin:4em;"></div>
							<div class="message"></div>						
						</div>						
						<div class="visible-xs" style="text-align:center">
							<h3 id="clock"></h3>
							<br>
						</div>
						<div class="sec-title text-center wow fadeInUp animated" data-wow-duration="700ms">
							<h4>Remaining Reguler Registration</h4>
						</div>
					</div>-->
					<div class="col-md-5">
						<div class="verticalCarousel">
							<div class="verticalCarouselHeader">
								<h3>News</h3>
								<a href="#" class="vc_goDown"><i class="fa fa-fw fa-angle-down"></i></a>
								<a href="#" class="vc_goUp"><i class="fa fa-fw fa-angle-up"></i></a>
							</div>
							<ul class="verticalCarouselGroup vc_list">
								@foreach($news as $new)
								<li>
									<h4>{!!$new->title_eng!!}</h4>
									<p>{!!$new->content_eng!!}</p>
								</li>
								@endforeach
							</ul>
						</div>						
					</div>
				</div>
			</div>				
		</section>		

		<section id="tentang-medspin" class="team">
			<div class="container">
				<div class="row">		
					<div class="sec-title text-center wow fadeInUp animated" data-wow-duration="700ms">
						<h2>What is MEDSPIN?</h2>
						<div class="devider"><i class="fa fa-heart-o fa-lg"></i></div>
					</div>

					<div class="col-md-4 wow fadeInRight animated" data-wow-duration="500ms">
						<img src="img/logo.png" alt="" class="img-responsive img-portfolio" style="margin-bottom:20px; margin-top:35px">
					</div>
					
					<div class="col-md-8">
						<div class="sec-sub-title wow fadeInRight animated" data-wow-duration="2000ms">
							<p style="text-align : justify">
								Medical Science and Application (MEDSPIN) Competition 2017 is an annual scientific competition held by Faculty of Medicine Airlangga University and MEDSPIN is the BIGGEST scientific and medical competition in Indonesia with more than 22.000 participants last year from all over Indonesia and other Southeastern Asian Countries. This competition is held for any Senior High School Students (grade X, XI, and XII) from every high school all across Southeast Asia.
							</p>
							<br/>
							<p style="text-align : justify">
								In 2017, Medspin will be held with a whole new theme, which is "Natural Disaster: Survival and Emergency Planning". Medspin 2017 consists of five rounds (Elimination, Quarter Final, Semifinal, Final, and Grand Final). The Elimination round will be held altogether in 30 cities all across Indonesia and online. The Quarter Final, Semifinal, Final, and Grand Final rounds will be held in Faculty of Medicine Airlangga University in Surabaya, Indonesia.
							</p>
							<br/>
							<p style="text-align : justify">
								By Medspin 2017, we hope to improve high school students' competence and to create a fun and vibrant environment for high school students to compete in.							
							</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		
		<section id="jadwal-perlombaan" class="features cd-horizontal-timeline">
			<div class="sec-title text-center mb50 wow bounceInDown animated" data-wow-duration="500ms">
				<h2>Schedule</h2>
				<div class="devider"><i class="fa fa-calendar fa-lg"></i></div>
			</div>
			<div class="hidden-xs timeline">
				<div class="events-wrapper">
					<div class="events">
						<ol>
							<li><a href="#0" data-date="01/01/2017" class="selected"><strong>Early Registration</strong></a></li>
							<li><a href="#0" data-date="01/04/2017" ><strong>Reguler</strong></a></li>							
							<li><a href="#0" data-date="01/07/2017" ><strong>Try Out 1</strong></a></li>														
							<li><a href="#0" data-date="01/09/2017" ><strong>Try Out 2</strong></a></li>																					
							<li><a href="#0" data-date="01/12/2017" ><strong>Tutup Pendaftaran</strong></a></li>																												
							<li><a href="#0" data-date="01/02/2018"><strong>Babak Penyisihan</strong></a></li>																												
							<li><a href="#0" data-date="30/05/2018"><strong>Babak Perempat Final &<br>Babak Semifinal</strong></a></li>
							<li><a href="#0" data-date="30/08/2018"><strong>Babak Final &<br>Babak Grand Final</strong></a></li>
						</ol>
						<span class="filling-line" aria-hidden="true"></span>
					</div> <!-- .events -->
				</div> <!-- .events-wrapper -->
					
				<ul class="cd-timeline-navigation" style="list-style: none">
					<li><a href="#0" class="prev inactive">Prev</a></li>
					<li><a href="#0" class="next">Next</a></li>
				</ul> <!-- .cd-timeline-navigation -->
			</div> <!-- .timeline -->

			<div class="hidden-xs events-content">
				<ol>
					<li class="selected" data-date="01/01/2017">
						<div class="service-desc">
							<h3 style="margin-bottom: 10px">17th July — 31st July 2017, Online</h3>
							<p style="margin-bottom: 10px"><strong>Early Registration</strong></p>
							<p>A designated span of Registration Term in which participators are given the previlege of <b>discounted registration fee</b>. Early registration may only be done online from the official website of Medspin 2017.</p>
						</div>						
					</li>
					<li data-date="01/04/2017">
						<div class="service-desc">
							<h3 style="margin-bottom: 10px">1st August — 31st October 2017, Online & on Each Designated Panwil</h3>
							<p style="margin-bottom: 10px"><strong>Reguler</strong></p>
							<p>The normal Registration Term with undiscounted registration fee. Registration is done online on the official website of Medspin 2017 and Offline on each designated <i>Panitia Wilayah</i> (<i>Panwil, plural: Panwils</i>).
								Offline registration at Kampus A FK UNAIR Surabaya are available at these hours: Weekdays except Fridays at 16.00—18.00 UTC+7; Fridays at 15.00—17.00 UTC+7; and Saturdays at 09.00—13.00 UTC+7.
							</p>
						</div>						
					</li>					
					<li data-date="01/07/2017">
						<div class="service-desc">
							<h3 style="margin-bottom: 10px">8th October 2017, Online</h3>
							<p style="margin-bottom: 10px"><strong>Try Out 1</strong></p>
							<p>The first training event which is provided for free by Medspin 2017. <i><u>This event is not obligatory for participants.</i></u> Participants practice solving questions to prepare for the Preliminary Round of Medspin 2017 (Grades from the <i>Try Outs</i> will not affect grades of the Preliminary Round).</p>
						</div>						
					</li>					
					<li data-date="01/09/2017">
						<div class="service-desc">
							<h3 style="margin-bottom: 10px">22nd October 2017, Online</h3>
							<p style="margin-bottom: 10px"><strong>Try Out 2</strong></p>
							<p>The second training event which is provided for free by Medspin 2017. <i><u>This event is not obligatory for participants. </i></u> Participants practice solving questions to prepare for the Preliminary Round of Medspin 2017 (Grades from the Try Outs will not affect grades of the Preliminary Round).</p>
						</div>						
					</li>															
					<li data-date="01/12/2017">
						<div class="service-desc">
							<h3 style="margin-bottom: 10px">31st Oktober 2017</h3>
							<p style="margin-bottom: 10px"><strong>End of Registration Term</strong></p>
							<p>The final minutes of your chance to participate in Medspin 2017.</p>
						</div>						
					</li>																				
					<li data-date="01/02/2018">
						<div class="service-desc">
							<h3 style="margin-bottom: 10px">5th November 2017, Online & on Each Designated Panwil</h3>
							<p style="margin-bottom: 10px"><strong>Preliminary Round</strong></p>
							<p>The first round of the series of contest in Medspin 2017. A test of the participants’ competence with Multiple Choice Questions (MCQ) with written exams at single locations designated by Medspin 2017, and Online exam on the exam module of the website of Medspin 2017 which are done concurrently at a designated time. The method of taking the test can be selected by participants during the registration term.</p>
						</div>						
					</li>							
					<li data-date="30/05/2018">
						<div class="service-desc">
							<h3 style="margin-bottom: 10px">18th November 2017, Faculty of Medicine UNAIR Surabaya</h3>
							<p style="margin-bottom: 10px"><strong>QuarterFinal Round</strong></p>
							<p>The second round of the series of contest in Medspin 2017. Participants show their skills in Essay Rallies and try to solve a medical riddle in the area of the Faculty of Medicine in Airlangga University Surabaya. This event takes place from 6:30 AM to 11:00 AM UTC+7.</p>			
							<br>						
							<p style="margin-bottom: 10px"><strong>SemiFinal Round</strong>	</p>									
							<p>The third round of the series of contest in Medspin 2017. The competence of the participants are tested again by essay problems about biology, physics, and chemistry theorical that are related to medical science. This event takes place at the Faculty of Medicine in Airlangga University Surabaya, from 6:00 AM to 11:00 AM UTC+7.</p>
						</div>						
					</li>														
					<li data-date="30/08/2018">
						<div class="service-desc">
							<h3 style="margin-bottom: 10px">19th November 2017, Faculty of Medicine UNAIR Surabaya</h3>
							<p style="margin-bottom: 10px"><strong>Final Round</strong>	</p>
							<p>The fourth round of the series of contest in Medspin 2017. Participants are faced with practical tests of biology, physics, chemistry, and applicative medicine science. Then participants solve long theorical essay problems of biology, physics, and chemistry. This event takes place at the Faculty of Medicine in Airlangga University Surabaya, from 6:30 AM to 11:00 AM UTC+7.</p>
							<br>
							<p style="margin-bottom: 10px"><strong>Grand Final</strong></p>									
							<p>The last round of the series of contest in Medspin 2017. The round that determines the winners of Medspin 2017 where participants make and do a presentation case problem-solving, and quick answer quizzes. This event takes place at the Faculty of Medicine in Airlangga University Surabaya, from 11:00 AM to 4:00 PM UTC+7.</p>
						</div>						
					</li>																					
				</ol>
			</div> <!-- .events-content -->
			<div class="visible-xs">
				<div class="container">
					<div class="row">						
						<div class="col-md-4 wow fadeInUp" data-wow-duration="500ms">
							<div class="service-item">
								<div class="service-icon">
									<i class="fa fa-pencil fa-2x"></i>
								</div>
								
								<div class="service-desc">
									<h3 style="margin-bottom: 10px">Early Registration</h3>
									<p style="margin-bottom: 10px"><strong>17th July—31st July 2017, Online</strong></p>
									<p>A designated span of Registration Term in which participators are given the previlege of <b>discounted registration fee</b>. Early registration may only be done online from the official website of Medspin 2017.</p>
								</div>
							</div>
						</div>
						<div class="col-md-4 wow fadeInUp" data-wow-duration="500ms">
							<div class="service-item">
								<div class="service-icon">
									<i class="fa fa-send fa-2x"></i>
								</div>
								
								<div class="service-desc">
									<h3 style="margin-bottom: 10px">Reguler</h3>
									<p style="margin-bottom: 10px"><strong>1st August — 31st October 2017, Online & on Each Designated Panwil</strong></p>
									<p>The normal Registration Term with undiscounted registration fee. Registration is done online on the official website of Medspin 2017 and Offline on each designated <i>Panitia Wilayah</i> (<i>Panwil, plural: Panwils</i>).
										Offline registration at Kampus A FK UNAIR Surabaya are available at these hours: Weekdays except Fridays at 16.00—18.00 UTC+7; Fridays at 15.00—17.00 UTC+7; and Saturdays at 09.00—13.00 UTC+7.</p>
								</div>
							</div>
						</div>			
						<div class="col-md-4 wow fadeInUp" data-wow-duration="500ms">
							<div class="service-item">
								<div class="service-icon">
									<i class="fa fa-pencil fa-2x"></i>
								</div>
								
								<div class="service-desc">
									<h3 style="margin-bottom: 10px">Try Out 1</h3>
									<p style="margin-bottom: 10px"><strong>8th October 2017, Online</strong></p>
									<p>The first training event which is provided for free by Medspin 2017. <i><u>This event is not obligatory for participants.</i></u> Participants practice solving questions to prepare for the Preliminary Round of Medspin 2017 (Grades from the <i>Try Outs</i> will not affect grades of the Preliminary Round).</p>
								</div>
							</div>
						</div>		
						<div class="col-md-4 wow fadeInUp" data-wow-duration="500ms">
							<div class="service-item">
								<div class="service-icon">
									<i class="fa fa-send fa-2x"></i>
								</div>
								
								<div class="service-desc">
									<h3 style="margin-bottom: 10px">Try Out 2</h3>
									<p style="margin-bottom: 10px"><strong>22nd October 2017, Online</strong></p>
									<p>The second training event which is provided for free by Medspin 2017. <i><u>This event is not obligatory for participants. </i></u> Participants practice solving questions to prepare for the Preliminary Round of Medspin 2017 (Grades from the Try Outs will not affect grades of the Preliminary Round).</p>
								</div>
							</div>
						</div>		
						<div class="col-md-4 wow fadeInUp" data-wow-duration="500ms">
							<div class="service-item">
								<div class="service-icon">
									<i class="fa fa-times fa-2x"></i>
								</div>
								
								<div class="service-desc">
									<h3 style="margin-bottom: 10px">End of Registration Term</h3>
									<p style="margin-bottom: 10px"><strong>31st October 2017</strong></p>
									<p>The final minutes of your chance to participate in Medspin 2017.</p>
								</div>
							</div>
						</div>		
						<div class="col-md-4 wow fadeInUp" data-wow-duration="500ms">
							<div class="service-item">
								<div class="service-icon">
									<i class="fa fa-pencil fa-2x"></i>
								</div>
								
								<div class="service-desc">
									<h3 style="margin-bottom: 10px">Preliminary Round</h3>
									<p style="margin-bottom: 10px"><strong>5th November 2017, Online & on Each Designated Panwil</strong></p>
									<p>The first round of the series of contest in Medspin 2017. A test of the participants’ competence with Multiple Choice Questions (MCQ) with written exams at single locations designated by Medspin 2017, and Online exam on the exam module of the website of Medspin 2017 which are done concurrently at a designated time. The method of taking the test can be selected by participants during the registration term.</p>
								</div>
							</div>
						</div>																					
						<div class="col-md-4 wow fadeInUp" data-wow-duration="1500ms">
							<div class="service-item">
								<div class="service-icon">
									<i class="fa fa-paper-plane fa-2x"></i>
								</div>
								
								<div class="service-desc">
									<h3 style="margin-bottom: 10px">QuarterFinal Round</h3>
									<p style="margin-bottom: 10px"><strong>18th November 2017, Faculty of Medicine UNAIR Surabaya</strong>	</p>
									<p>The second round of the series of contest in Medspin 2017. Participants show their skills in Essay Rallies and try to solve a medical riddle in the area of the Faculty of Medicine in Airlangga University Surabaya. This event takes place from 6:30 AM to 11:00 AM UTC+7.</p>
								</div>
							</div>
						</div>
					</div>
					
					<div class="row" style="margin-top:20px">

						<div class="col-md-4 wow fadeInUp" data-wow-duration="2000ms">
							<div class="service-item">
								<div class="service-icon">
									<i class="fa fa-pencil fa-2x"></i>
								</div>
								
								<div class="service-desc">
									<h3 style="margin-bottom: 10px">SemiFinal Round</h3>
									<p style="margin-bottom: 10px"><strong>18th November 2017, Faculty of Medicine UNAIR Surabaya</strong>	</p>
									<p>The third round of the series of contest in Medspin 2017. The competence of the participants are tested again by essay problems about biology, physics, and chemistry theorical that are related to medical science. This event takes place at the Faculty of Medicine in Airlangga University Surabaya, from 6:00 AM to 11:00 AM UTC+7.</p>
								</div>
							</div>
						</div>

						<div class="col-md-4 wow fadeInUp" data-wow-duration="2500ms">
							<div class="service-item">
								<div class="service-icon">
									<i class="fa fa-flask fa-2x"></i>
								</div>
								
								<div class="service-desc">
									<h3 style="margin-bottom: 10px">Final Round</h3>
									<p style="margin-bottom: 10px"><strong>19th November 2017, Faculty of Medicine UNAIR Surabaya</strong>	</p>
									<p>The fourth round of the series of contest in Medspin 2017. Participants are faced with practical tests of biology, physics, chemistry, and applicative medicine science. Then participants solve long theorical essay problems of biology, physics, and chemistry. This event takes place at the Faculty of Medicine in Airlangga University Surabaya, from 6:30 AM to 11:00 AM UTC+7.</p>
								</div>
							</div>
						</div>

						<div class="col-md-4 wow fadeInUp" data-wow-duration="3000ms">
							<div class="service-item">
								<div class="service-icon">
									<i class="fa fa-slideshare fa-2x"></i>
								</div>
								
								<div class="service-desc">
									<h3 style="margin-bottom: 10px">Grand Final</h3>
									<p style="margin-bottom: 10px"><strong>19th November 2017, Faculty of Medicine UNAIR Surabaya</strong>	</p>
									<p>The last round of the series of contest in Medspin 2017. The round that determines the winners of Medspin 2017 where participants make and do a presentation case problem-solving, and quick answer quizzes. This event takes place at the Faculty of Medicine in Airlangga University Surabaya, from 11:00 AM to 4:00 PM UTC+7.</p>
								</div>
							</div>
						</div>
		
					</div>
				</div>
			</div>
		</section>
		
		<section id="registrasi" class="works clearfix">
			<div class="container">
				<div class="row">
				
					<div class="sec-title text-center">
						<h2>Registration</h2>
						<div class="devider"><i class="fa fa-heart-o fa-lg"></i></div>
					</div>

					<div class="sec-sub-title text-center">
						<p>
							Registration Fee : <strong style="color:#386570">USD $20</strong>/Team for International Participants and <strong style="color:#386570">IDR 135.000</strong>/Team for Indonesian Participants
						</p>
					</div>
										
					<div class="work-filter wow fadeInRight animated" data-wow-duration="500ms" style="margin-top:20px">
						<ul class="text-center">
							<li><a href="javascript:;" class="active btns" onclick="$('.btns').removeClass('active'); $(this).addClass('active'); $('.registration').slideUp(); $('#persyaratan-umum').slideDown();">General Requirements</a></li>
							<li><a href="javascript:;" class="btns" onclick="$('.btns').removeClass('active'); $(this).addClass('active'); $('.registration').slideUp(); $('#registrasi-online').slideDown();">Online Registration</a></li>
							<li><a href="javascript:;" class="btns" onclick="$('.btns').removeClass('active'); $(this).addClass('active'); $('.registration').slideUp(); $('#registrasi-offline').slideDown();">Offline Registration</a></li>
							<li><a href="javascript:;" class="btns" onclick="$('.btns').removeClass('active'); $(this).addClass('active'); $('.registration').slideUp(); $('#silabus').slideDown();">Syllabus</a></li>
							<li><a href="javascript:;" class="btns" onclick="$('.btns').removeClass('active'); $(this).addClass('active'); $('.registration').slideUp(); $('#hadiah').slideDown();">Prizes</a></li>
						</ul>
					</div>
					
				</div>
			</div>
			
			<div class="container">

				<div class="row">
					<div class="col-md-5">
						<img src="img/medspin-1.jpg" alt="" class="img-responsive img-portfolio" style="margin-bottom:20px;">
					</div>
					<div class="col-md-7">			
						<div class="sec-sub-title registration" id="persyaratan-umum" style="margin-top:0px">
							<ol style="margin-top:0px">
								<li>The participants of Medspin 2017 are the students of Senior High School / of the same level from the 1st, 2nd, and/or 3rd grade, be it from public or private school in all areas of Southeast Asia.</li>
								<li>The participants of Medspin 2017 are teams that include for each team three students of Senior High School / of the same level, be it from the same or different grade. In this case, the participants have to follow the competition as representatives of the same school under the condition that each school is allowed to send more than one team.</li>
								<li>The student who cannot make it to the competition and be replaced by another student without informing will be disqualified.</li>
								<li>A team is still allowed to follow the elimination round with the total minimum of two students present. But, this case will not be permitted in the quarter final round and other rounds. If it happens in the quarter final round and other rounds, then the team will be disqualified.</li>																
							</ol>
							<br>
							<a class="btn btn-primary" href="https://drive.google.com/file/d/0B1WOK5SykYHFSXZhVkRJZlhyWUk/view?usp=drivesdk" target="__blank">Download Medspin 2017 Guidelines</a>							
						</div>
						<div class="sec-sub-title registration" id="registrasi-online" style="margin-top:0px; display:none">
							<ol style="margin-top:0px">
								<b>Requirements :</b>
								<li>Complete and submit the registration form  <a href="http://pendaftaran.medspinfkunair.com/" target="__blank" class="btn btn-success" role="button" style="background-color:#275864; border-color:#275864; margin-bottom:10px">here</a>.</li>
								<li>Attach a soft copy (scan) of a student ID for each team member.</li>
								<li>Attach a 3x4 passport photo with the name of each participant as the file name.</li>
								<li>Complete the payment of the registration fee (IDR Rp135.000/team for Indonesian participants, IDR Rp125.000/team for early bird registration, and USD $20/team for international participants) and attach your proof of payment/transfer receipt.</li>
							</ol>
							<br>
							<ol style="margin-top:0px">
								<b>Registration Process :</b>
								<li>Online registration will be open from July 17th – July 31st 2017 for early registration, and August 1st – October 31st 2017 for regular registration. To determine any time-related issues during registration, we will be referring to the digital clock displayed on our website.</li>
								<li>All registrations must be completed on the official website of Medspin 2017, <a href="http://medspinfkunair.com/en" target="__blank" class="btn btn-success" role="button" style="background-color:#275864; border-color:#275864; margin-bottom:10px">http://www.medspinfkunair.com/en</a></li>
								<li>An account on our website must be created in order to complete the online registration process.</li>
								<li>Pay the registration fee (IDR Rp135.000/team for Indonesian participants, IDR Rp125.000/team for early bird registration, and USD $20/team for international participants) to Medspin 2017’s bank account (Bank Mandiri 141 00 1143426 3) and await further confirmation. Payment confirmation will be given within the next 24 hours.</li>
								<li>Complete the registration form, and confirm that the data is correct. Once confirmed, the registration form cannot be changed.</li>
								<li>Participants of the offline preliminary rounds are obliged to download and print their proof of online registration, and must bring their proof of online registration to re-registration.</li>
								<li>All administrative documents and issues will only be accepted with proper proof of payment/transfer receipts to Medspin 2017’s official bank account.</li>
							</ol>							
						</div>		
						<div class="sec-sub-title registration" id="registrasi-offline" style="margin-top:0px; display:none">
							<p class="registration">
								<b>Requirements :</b>
								<ul style="margin-top:0px">
									<li>Complete and submit the registration form. Participants can obtain registration forms from referred schools, teachers appointed as registration coordinators, Medspin 2017’s committee, or by downloading the form from Medspin 2017’s official website: <a href="http://medspinfkunair.com/" target="__blank" class="btn btn-success" role="button" style="background-color:#275864; border-color:#275864; margin-bottom:10px">http://www.medspinfkunair.com</a></li> 
									<li>Submit photocopies of valid student ID’s for each team member. Each member needs to submit 1 photocopy.</li>
									<li>Submit a recent 3x4 passport photo. Photos can be in black and white or in color. Each member must submit 1 photo, with identity details written on the back of the photo.</li>
									<li>Provide proof of document completion that will be filled in by the committee of Medspin 2017. The proof of document completion will be in the form of one set of documents that will be given to each participant. Participants must bring their proof of document completion during re-registration in order to receive their participant kit.</li>
									<li>Pay the registration fee (IDR Rp135.000/team for regular registration, IDR 125.000/team for early registration) to the Medspin 2017 committee. After completion of payment, participants will receive a receipt that must be brought during re-registration in order to receive the participant kit.</li>
								</ul>	
							</p>
							<br>
							<ol style="margin-top:0px">
								<b>Registration Process : </b>
								<li>Complete the Medspin 2017 registration form..<br> 
								<li>Submit the registration fee and required documents from July 17th – July 31st 2017 for early registration, and August 1st – October 31st 2017 for regular registration to Medspin 2017 Secretariat, which can be found at:
									<ul>
										<li>For participants residing in Surabaya, offline registration can be done at Gazebo Tengah (Gazteng) of Faculty of Medicine of Airlangga University, Jl. Mayjend. Prof. Dr. Moestopo 47 Surabaya (Campus A)</li>
											<ul>
												<li>Monday to Friday from 03.00 pm until 05.00 pm.</li>
												<li>Saturday from 09.00 am until 01.00 pm.</li>
											</ul>
										<li>For participants outside of Surabaya, offline registration can be completed through Medspin 2017’s Regional Committee. Contact details for the committee can be found at the end of this document.</li>
									</ul>
								</li>
								<li>Participants must bring their payment receipt and proof of document completion during re-registration in order to receive their participant kit. The date of re-registration will be announced soon.</li>							
							</ol>
							<a href="https://drive.google.com/file/d/0B1WOK5SykYHFMEJOeWxMYnlSTmM/view?usp=drivesdk" target="__blank" class="btn btn-success" role="button" style="background-color:#275864; border-color:#275864; margin-top:15px"><i class="fa fa-cloud-download" aria-hidden="true"></i> Offline Registration Form</a>
						</div>
						<div class="sec-sub-title registration" id="silabus" style="margin-top:0px; display:none">
							<a href="https://drive.google.com/drive/folders/0B8g-yuRVnzjnZWpybnY5ejJkTzg?usp=sharing" target="__blank" class="btn btn-success" role="button" style="background-color:#275864; border-color:#275864; margin-bottom:10px"><i class="fa fa-cloud-download" aria-hidden="true"></i> Syllabus</a>
							<a href="https://drive.google.com/drive/folders/0B8g-yuRVnzjnODFocGRPTW9WY28?usp=sharing" target="__blank" class="btn btn-success" role="button" style="background-color:#275864; border-color:#275864; margin-bottom:10px"><i class="fa fa-cloud-download" aria-hidden="true"></i> Example Question</a>
						</div>
						<div class="sec-sub-title registration" id="hadiah" style="margin-top:0px; display:none">
							<div class="col-md-4 col-sm-6 col-xs-12 text-center">
								<div class="counters-item">
									<i class="fa fa-trophy fa-3x"></i>
									<strong>1<sup>st</sup></strong>
									<p>
										Rp9.000.000,-<br>
										Piala Menristekdikti<br>
										& <br>
										Piala Medspin
									</p>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12 text-center">
								<div class="counters-item">
									<i class="fa fa-trophy fa-3x"></i>
									<strong>2<sup>nd</sup></strong>
									<p>
										Rp7.000.000,-<br>
										Piala Gubernur Jawa Timur<br>
										& <br>
										Piala Medspin
									</p>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12 text-center">
								<div class="counters-item">
									<i class="fa fa-trophy fa-3x"></i>
									<strong>3<sup>rd</sup></strong>
									<p>
										Rp5.000.000,-<br>
										Piala Walikota<br>
										& <br>
										Piala Medspin
									</p>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12 text-center">
								<div class="counters-item">
									<i class="fa fa-trophy fa-3x"></i>
									<strong>4<sup>th</sup></strong>
									<p>
										Rp2.500.000,-<br>
										Piala Medspin 2017
									</p>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12 text-center">
								<div class="counters-item">
									<i class="fa fa-trophy fa-3x"></i>
									<strong>5<sup>th</sup></strong>
									<p>
										Rp1.500.000,-<br>
										Piala Medspin 2017
									</p>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12 text-center">
								<div class="counters-item">
									<i class="fa fa-trophy fa-3x"></i>
									<strong><sup>Best</sup>Theory</strong>
									<br>
									<p>
										Rp300.000,-<br>
									</p>
								</div>
							</div>							
							<div class="col-md-4 col-sm-6 col-xs-12 text-center">
								<div class="counters-item">
									<i class="fa fa-trophy fa-3x"></i>
									<strong><sup>Best</sup>Experiment</strong>
									<p>
										Rp300.000,-<br>
									</p>
								</div>
							</div>														
						</div>
					</div>
				</div>
				
			</div>
		
		</section>

		<section id="twibbon" class="team">
			<div class="container">
				<div class="row">		
					<div class="sec-title text-center wow fadeInUp animated" data-wow-duration="700ms">
						<h2>Twibbon MEDSPIN 2017</h2>
						<div class="devider"><i class="fa fa-heart-o fa-lg"></i></div>
					</div>

					<div class="col-md-4 wow fadeInRight animated" data-wow-duration="500ms">
						<img src="img/twibbon.jpg" alt="" class="img-responsive img-portfolio" style="margin-bottom:20px; margin-top:35px">
						<a class="btn btn-primary pull-right" href="img/twibbon.jpg" download="twibbon-image">Download Twibbon Image</a>
					</div>
					
					<div class="col-md-8">
						<div class="sec-sub-title wow fadeInRight animated" data-wow-duration="2000ms">
							<p style="text-align : justify">
								DID YOU KNOW 
							</p>
							<br/>
							<p style="text-align : justify">
								Being part of Asean Economic Community (AEC), Indonesia also loaded with sea of opportunities for the people to expand their bussiness and be more successful at. Unfortunately, a research conducted to students Indonesia found that only 53 of 100 pupils are ready for competition which soon they encounter, especially in AEC era.
							</p>
							<p style="text-algin : justify">
								A famous man, Thomas Edison, known for his lightbulb invention after 1000 of unsuccessful trials once says "Opportunity is missed by most people because it is dressed in overalls and looks like hardwork"
							</p>	
							<p style="text-align : justify">
								Therefore, 
							</p>
							<p style="text-align : justify">
								I, (name), from (school) am taking my opportunity and ready to compete with thousands of student from Indonesia and ASEAN countries in the biggest medical competition in Indonesia, MEDSPIN 2017! 							
							</p>	
							<br/>
							<p style="text-algin : justify">
								-I am part of the 53, are you?
							</p>
							<p style="text-algin : justify">
								-I am ready, are you?							
							</p>							
							<br/>
							<p style="text-algin : justify">
								#BeAMedicalHero
							</p>							
							<p style="text-algin : justify">
								#Medspin2017
							</p>							
							<p style="text-algin : justify">
								#FKUnair
							</p>		
							<br/>																			
							<p style="text-algin : justify">
								Tag 5 of your friends in your social media (Instagram, Facebook, Line, etc)
							</p>														
							<p style="text-algin : justify">
								-to let them know?
							</p>														
							<p style="text-algin : justify">
								-to share cause you care 💕
							</p>																												
						</div>
					</div>
				</div>
			</div>
		</section>		
		
		<section id="region-penyisihan" class="features">
			<div class="container">
				<div class="row">
				
					<div class="sec-title text-center mb50 wow bounceInDown animated" data-wow-duration="500ms">
						<h2>Panwil</h2>
						<div class="devider"><i class="fa fa-map-marker fa-lg"></i></div>
					</div>

					<div class="sec-sub-title text-center">
						<img src="img/map.png" alt="" class="img-responsive img-portfolio image-center" style="margin-bottom:20px;">
					</div>
			
					@foreach($regions as $i => $region)

						@if($region->city == '-')
						@else
						<div class="col-md-2 col-xs-6 wow fadeInUp" data-wow-duration="500ms">
							<button type="button" data-toggle="modal" data-target="#region-{{$i}}" class="btn btn-success" style="background-color:#275864; border-color:#275864; width:100%; margin-bottom:10px">{{$region->city}}</button>
						</div>
						@endif

					@endforeach
	
				</div>
			</div>
		</section>

		<section id="galeri" class="works clearfix">
			<div class="container">
				<div class="row">
					<div class="sec-title text-center">
						<h2>Medspin Gallery 2016</h2>
						<div class="devider"><i class="fa fa-heart-o fa-lg"></i></div>
					</div>
				</div>
			</div>
			<div class="project-wrapper" style="margin-top:50px">
				@for($i=1; $i<9; $i++)
				<figure class="mix work-item branding"><img src="img/medspin-{{$i}}.jpg">
					<figcaption class="overlay"><a class="fancybox" href="img/medspin-{{$i}}.jpg"><i class="fa fa-eye fa-lg"></i></a>
					</figcaption>
				</figure>
				@endfor
			</div>
			<iframe class="video-teaser" frameborder="0"  src="https://www.youtube.com/embed/_MhnzdevFFc" allowfullscreen></iframe>
		</section>

		<!-- <section id="sponsors" class="contact">
			<div class="container">
				<hr>
				<div class="row mb50">														
					<h3 style="text-align: center">For Sponsor Inquires Please Contact :</h3>
					<br>
					<div align="center" class="col-lg-6 col-md-6 col-sm-7 col-xs-12 wow fadeInDown animated" data-wow-duration="500ms" data-wow-delay="300ms">
						<div class="contact-address">
							<h4 style="margin-top:10px">Ayu Rahmanita Putri</h4>
							<p>Email : ayu7485@live.com atau putriayu040998@gmail.com</p>
							<p>WhatasApp : 085649729709</p>
						</div>
					</div>
					<div align="center" class="col-lg-6 col-md-6 col-sm-7 col-xs-12 wow fadeInDown animated" data-wow-duration="500ms" data-wow-delay="300ms">
						<div class="contact-address">
							<h4 style="margin-top:10px">Eka Candra Setiawan</h4>
							<p>Email : ekacandrasetyawan@gmail.com</p>
							<p>WhatasApp : 081252324137</p>							
						</div>
					</div>															
				</div>
				<hr>
			</div>			
		</section>		 -->
		
		<section id="tanya-medspin" class="contact">
			<div class="container">
				<div class="row mb50">
				
					<div class="sec-title text-center mb50 wow fadeInDown animated" data-wow-duration="500ms">
						<h2>ASK MEDSPIN</h2>
						<div class="devider"><i class="fa fa-question fa-lg"></i></div>
						<p>There’s a saying in Indonesia which roughly translates as <i>“not knowing then no loving.”</i> So let’s get acquainted with us and ask stuff about MEDSPIN 2017 using all available media listed below. All so that we could hold each other dear to ourselves..</p>
					</div>
										
					<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 wow fadeInLeft animated" data-wow-duration="500ms">
						<div class="contact-address">
							<h3 style="margin-bottom:10px">Sekretariat MEDSPIN</h3>
							<p>Student Center GRABIK IPTEKDOK FK UNAIR</p>
							<p>Jalan Mayjend Prof. Dr. Moestopo nmr. 47 Surabaya, Postcode 60131</p>
							<p>Contact Person :</p>
							<p>Ms. Qonita (+6281230391660)</p>
							<p>Ms. Devina (+6281252930000)</p>
							<p>Line : @CUG8195X</p>
							<p>E-mail : medspin2017@gmail.com</p>
						</div>
					</div>
					
					<div class="col-lg-8 col-md-8 col-sm-7 col-xs-12 wow fadeInDown animated" data-wow-duration="500ms" data-wow-delay="300ms">
						<div id="disqus_thread"></div>
						<script>
						var disqus_config = function () {
						this.page.url = medspinfkunair.com;
						this.page.identifier = 'medspin-id';
						};
						(function() { 
						var d = document, s = d.createElement('script');
						s.src = 'https://medspin-2017.disqus.com/embed.js';
						s.setAttribute('data-timestamp', +new Date());
						(d.head || d.body).appendChild(s);
						})();
						</script>
						<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
					</div>
					
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-12 wow fadeInRight animated" data-wow-duration="500ms" data-wow-delay="600ms">
						<ul class="footer-social">
							<li><a href="https://web.facebook.com/Medspin-FK-Unair-2017-669727393093938/?fref=ts"><i class="fa fa-facebook fa-2x"></i></a></li>
							<li><a href="https://www.instagram.com/medspinfkunair/?hl=id"><i class="fa fa-instagram fa-2x"></i></a></li>
							<li><a href="https://www.youtube.com/channel/UClhaRq4HolNot70zUxGr0Bg"><i class="fa fa-youtube fa-2x"></i></a></li>
						</ul>
					</div>
					
				</div>
			</div>
			
			<div id="map_canvas" class="wow bounceInDown animated" data-wow-duration="500ms"></div>
			
		</section>
		
		
		<footer id="footer" class="footer" style="padding: 10px 0;">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<p class="copyright text-center">
							Copyright © MEDSPIN 2017. Powered by <a href="https://www.olimpiade.id/">Olimpiade.id</a>
						</p>
					</div>
				</div>
			</div>
		</footer>
		
		<a href="javascript:void(0);" id="back-top"><i class="fa fa-angle-up fa-3x"></i></a>

        <script src="js/jquery-1.11.1.min.js"></script>

        <script src="js/jquery.singlePageNav.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.fancybox.pack.js"></script>
        <script src="js/jquery.mixitup.min.js"></script>
        <script src="js/jquery.parallax-1.1.3.js"></script>
        <script src="js/jquery-countTo.js"></script>
        <script src="js/jquery.appear.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/3.32/jquery.form.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.11.1/jquery.validate.min.js"></script>
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
        <script src="js/jquery.easing.min.js"></script>
        <script src="js/wow.min.js"></script>
		<script src="js/jquery.mobile.custom.min.js"></script>
		<script src="js/main.js"></script> <!-- Resource jQuery -->		
        <script src="js/jQuery.verticalCarousel.js"></script>
		<script src="js/flipclock.js"></script>		
		<script src="js/jquery.countdown.js"></script>				


		<script type="text/javascript">
			$(document).ready(function(){
				$("#newsModal").modal('show');
			});
		</script>
		<!-- News Modal -->
		<div id="newsModal" class="modal fade" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">News</h4>
				</div>
				<div class="modal-body">
					<p>
						Hello Medical Hero! 
						<br/>
						Selamat yaa bagi kalian yang lolos ke quarterfinal. Dan bagi kalian yang belum lolos, jangan berkecil hati! Tetap semangat dan ikutilah lomba-lomba yang lain
						<br/>
						Berikut adalah file-file yang harus didownload bagi tim yang lolos ke quarterfinal:
						<br/>	
						<ul style="margin-left: 20px">
							<li>
								<a target="__blank" href=" https://drive.google.com/open?id=1G6D6r61h1b0y7cg3hE_CWXRbtSCt4RwU">Formulir Registrasi</a>
							</li>
							<li>
								<a target="__blank" href=" https://drive.google.com/open?id=1GbcLfdn7122eAQYMNN7GBhtwNVig8mtr">Guide Book</a>
							</li>
							<li>
								<a target="__blank" href="https://drive.google.com/open?id=1504Ju9iX4UNd028nYOiXCoHdip3fgq-s">Pengumuman 150 besar</a>
							</li>
						</ul>
					</p>
					<br>
					<p>
						#BeAMedicalHero
						#Medspin2017
						#FKUnair						
					</p>					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
				</div>

			</div>
		</div>

        <script>
            $(".verticalCarousel").verticalCarousel({
                currentItem: 1,
                showItems: 2,
            });
        </script><script type="text/javascript">
		<script>
			var wow = new WOW ({
				boxClass:     'wow',      
				animateClass: 'animated', 
				offset:       120,          
				mobile:       false,      
				live:         true        
			  }
			);	
			wow.init();
		</script> 
        <script src="js/custom.js"></script>
		
		<script type="text/javascript">			
			$(document).ready(function() {
				var clock;
				var date  = new Date('10/31/2017 5:00:01 PM UTC');
				var now   = new Date();
				// window.alert(date);
				var diff  = date.getTime()/1000 - now.getTime()/1000;

				clock = $('.clock').FlipClock(diff, {
					clockFace: 'DailyCounter',
					autoStart: false,
					callbacks: {
						stop: function() {
							$('.message').html('The clock has stopped!')
						}
					}
				});						
				clock.setCountdown(true);
				clock.start();
			});
		</script>
		<script>
			$('#clock').countdown('2017/11/01', function(event) {
			$(this).html(event.strftime('%D days %H:%M:%S'));
			});		
		</script>

		@foreach($regions as $i => $region)
		<div id="region-{{$i}}" class="modal fade" role="dialog">
			<div class="modal-dialog">

				<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Panwil {{$region->city}}</h4>
				</div>
				<div class="modal-body">
					<p> <strong><h4>{{$region->tempat}}</h4></strong> </p>
					<br>
					<p><h4>Kontak : {{$region->pic}}</h4></p>					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
				</div>

			</div>
		</div>
		@endforeach
    </body>
</html>

