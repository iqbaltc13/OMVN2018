<!DOCTYPE html>
<html lang="id">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>OMVN 2018 The Glory Of Numbers</title>		
        <meta name="description" content="Olimpiade Matematika “Vektor” Nasional merupakan Olimpiade Matematika yang diselenggarakan oleh Himpunan Mahasiswa Jurusan Matematika “Vektor” Universitas Negeri Malang untuk siswa jenjang SD, SMP dan SMA atau sederajat yang pada tahun 2018 ini diselenggarakan di 37 Rayon dengan 42 tempat pelaksanaan yang tersebar di seluruh Indonesia. Daftar rayon dan tempat pelaksanaan selengkapnya dapat dilihat di website www.omvn-um.org">
        <meta name="keywords" content="lomba, sains, olimpiade matematika, sains, olimpiade sains, matematika, um, universitas negeri malang, kompetisi, malang">
        <meta name="author" content="Jurusan Matematika Universitas Negeri Malang">
        <meta name="viewport" content="width=device-width, initial-scale=1">

		<link rel="icon" type="image/png" href="img/logo.png">
				
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>

      <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/jquery.fancybox.css">
        <link rel="stylesheet" href="css/animate.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/media-queries.css">
		<link rel="stylesheet" href="css/style-timeline.css">
        <link rel="stylesheet" href="css/style-list.css">
        <link rel="stylesheet" href="css/jQuery.verticalCarousel.css">		
		<link rel="stylesheet" href="css/flipclock.css">
		<link rel="stylesheet" href="css/font-awesome.css">
		 
        <script src="js/modernizr-2.6.2.min.js"></script>
    </head>
	
    <body id="body">
		<div id="preloader">
			<img src="img/preloader.gif" alt="Preloader" class="heavy-rotation">
		</div>
	
        <header id="navigation" class="navbar-fixed-top navbar">
            <div class="container">			
                <div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <i class="fa fa-bars fa-2x"></i>
                    </button>
					
                    <a class="navbar-brand" href="#" style="margin-left:10px">
						<h1 id="logo">
							<img src="img/logo.png" alt="Brandi" style="height: 45px; margin-top: 3px;">
						</h1>
					</a>
                </div>								

                <nav class="collapse navbar-collapse navbar-right" role="navigation">
                    <ul id="nav" class="nav navbar-nav">
                        <li><a href="#tentang-medspin">About Us</a></li>
                        <li><a href="#region-penyisihan">Region</a></li>
                       
                        <li><a href="#registrasi">Registration</a></li>
                         <li><a href="#jadwal-perlombaan">Timeline</a></li>
                        
                        <li><a href="#galeri">Galeri</a></li>
                        <li><a href="#tryout">Try Out</a></li>
						<li><a href="#tanya-medspin">Contact Us</a></li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Silabus dan Contoh Soal<span class="caret"></span></a>
							<ul id="dropdown-navbar" class="dropdown-menu">
								<li>
									<a target="_blank" href="https://drive.google.com/drive/folders/1EqgLHTVyH8HHrRyUXR8AWxZ2unTMmd3i?usp=sharing" style="text-color: black;">Referensi Buku dan Silabus</a>
								</li>
								<li>
									<a target="_blank" href="https://drive.google.com/file/d/1ORot2touPlt-R0SuxIqOM5tSq7Cqzwav/view?usp=sharing" style="text-color: black;" target="__blank">Kumpulan Soal OMVN</a>									
								</li>
							</ul>
						</li>
                    </ul>
					<a href="{{url('/')}}"><img src="img/flag-indo.png" alt="indo-lang" style="height: 20px; margin-top: 15px;"></a>							
					<a href="{{url('/en')}}"><img src="img/flag-uk.png" alt="uk-lang" style="height: 20px; margin-top: 15px;"></a>							
                </nav>				
            </div>
        </header>
       
		@include('partials.header-image')
 
		

		<section id="tentang-medspin" class="team">
			<div class="container">
				<div class="row">		
					<div class="sec-title text-center wow fadeInUp animated" data-wow-duration="700ms">
						<h2>Apa Itu OMVN?</h2>
						<div class="devider"><i class="fa fa-heart-o fa-lg"></i></div>
					</div>

					<div class="col-md-4 wow fadeInRight animated" data-wow-duration="500ms">
						<img src="img/logo.png" alt="" class="img-responsive img-portfolio" style="margin-bottom:20px; margin-top:35px">
					</div>
					
					<div class="col-md-8">
						<div class="sec-sub-title wow fadeInRight animated" data-wow-duration="2000ms">
							<p style="text-align : justify">
								Olimpiade Matematika “Vektor” Nasional merupakan Olimpiade Matematika yang diselenggarakan oleh Himpunan Mahasiswa Jurusan Matematika “Vektor” Universitas Negeri Malang untuk siswa jenjang SD, SMP dan SMA atau sederajat yang pada tahun 2018 ini diselenggarakan di 37 Rayon dengan 42 tempat pelaksanaan yang tersebar di seluruh Indonesia. Daftar rayon dan tempat pelaksanaan selengkapnya dapat dilihat di website www.omvn-um.org
							</p>
							
						</div>
					</div>
				</div>
			</div>
		</section>
		 
        <h3 class="mbr-section-subtitle pb-5 mbr-fonts-style display-5">
            
        </h3>
        <section id="region-penyisihan" class="features">
            <div class="container">
                <div class="row">
                
                    <div class="sec-title text-center mb50 wow bounceInDown animated" data-wow-duration="500ms">
                        <h2>Panwil Penyisihan</h2>
                        <div class="devider"><i class="fa fa-map-marker fa-lg"></i></div>
                    </div>
                    <div class="col-md-12">
                        <div class="sec-sub-title text-center">
                            <img src="img/map.png" alt="" class="img-responsive img-portfolio image-center" style="margin-bottom:20px;">
                        </div>
                </div>

                    <br/>
                    @foreach($regions as $i => $region)

                        @if($region->city == '-')
                        @else
                        <div class="col-md-2 col-xs-6 wow fadeInUp" data-wow-duration="500ms">
                            <button type="button" data-toggle="modal" data-target="#region-{{$i}}" class="btn btn-success" style="background-color:#c4c635; border-color:#c4c635; width:100%; margin-bottom:10px">{{$region->city}}</button>
                        </div>
                        @endif

                    @endforeach
    
                </div>
            </div>
        </section>
        <section id="registrasi" class="works clearfix">
            <div class="container">
                <div class="row">
                
                    <div class="sec-title text-center">
                        <h2>Registrasi Peserta</h2>
                        <div class="devider"><i class="fa fa-heart-o fa-lg"></i></div>
                    </div>

                    <!-- <div class="sec-sub-title text-center">
                        <p>
                            Biaya pendaftaran sebesar  <strong style="color:#386570">Rp150.000,-</strong>/Kelompok
                        </p>
                    </div> -->
                                        
                    <div class="work-filter wow fadeInRight animated" data-wow-duration="500ms" style="margin-top:20px">
                        <ul class="text-center">
                            <li><a href="javascript:;" class="active btns" onclick="$('.btns').removeClass('active'); $(this).addClass('active'); $('.registration').slideUp(); $('#persyaratan-umum').slideDown();">Persyaratan Umum</a></li>
                            <li><a href="javascript:;" class="btns" onclick="$('.btns').removeClass('active'); $(this).addClass('active'); $('.registration').slideUp(); $('#registrasi-online').slideDown();">Registrasi Online</a></li>
                            <li><a href="javascript:;" class="btns" onclick="$('.btns').removeClass('active'); $(this).addClass('active'); $('.registration').slideUp(); $('#registrasi-offline').slideDown();">Registrasi Offline</a></li>
                            <li><a href="javascript:;" class="btns" onclick="$('.btns').removeClass('active'); $(this).addClass('active'); $('.registration').slideUp(); $('#fasil-biaya').slideDown();">Fasilitas  dan Biaya Pendaftaran</a></li>
                            <li><a href="javascript:;" class="btns" onclick="$('.btns').removeClass('active'); $(this).addClass('active'); $('.registration').slideUp(); $('#silabus').slideDown();">Silabus dan Contoh Soal</a></li>
                            <li><a href="javascript:;" class="btns" onclick="$('.btns').removeClass('active'); $(this).addClass('active'); $('.registration').slideUp(); $('#hadiah').slideDown();">Hadiah</a></li>
                        </ul>
                    </div>
                    
                </div>
            </div>
            
            <div class="container">

                <div class="row">
                    <div class="col-md-5">
                        <img src="assets/images/OMVN-1.JPG" alt="" class="img-responsive img-portfolio" style="margin-bottom:20px;">
                    </div>
                    <div class="col-md-7">          
                        <div class="sec-sub-title registration" id="persyaratan-umum" style="margin-top:0px">
                            <ol style="margin-top:0px">
                                <li>Peserta OMVN 2018 merupakan siswa/siswi SD, SMP, dan SMA atau sederajatdi seluruh Indonesia dengan kategori perlombaan seperti berikut:
                                <p>a.   SD  : Perorangan</p>
                                <p>b.   SMP : Perorangan dan beregu</p>
                                <p>c.   SMA : Perorangan dan beregu</p>
                                </li>
                                <li>Peserta OMVN 2018 terdiri dari peserta beregu dan perorangan. Satu regu dihitung sebagai 1 (satu) peserta.</li>
                                <li>Satu regu terdiri dari tiga orang.</li>
                                <li>Peserta beregu harus berasal dari sekolah yang sama</li>
                                <li>Peserta beregu hanya boleh menjadi anggota satu regu</li>
                                <li>Peserta SMP dan SMA diperbolehkan mendaftarkan diri sebagai peserta beregu dan/atau perorangan</li>
                                <li>Peserta diperbolehkan mendaftarkan diri sebagai delegasi (perwakilan) sekolah atau mandiri (pribadi)</li>
                                <li>Peserta diperbolehkan mendaftarkan diri sebagai delegasi (perwakilan) sekolah atau mandiri (pribadi)</li>
                                <li>Peserta mendaftar sesuai jenjang pendidikan yang sedang ditempuh atau jenjang pendidikan yang lebih tinggi</li>
                                <li>Masing-masing sekolah dapat mendelegasikan lebih dari satu peserta</li>
                                <li>Anggota regu yang berhalangan hadir tidak dapat digantikan dengan orang lain dan nilai anggota regu tersebut dianggap 0.</li>
                            </ol>
                            <br>
                            <a class="btn btn-primary" href="https://drive.google.com/file/d/1TVWp8YoRDopUUVH_EEs9drv-Ijd6m8jk/view?usp=sharing" target="__blank">Download Petunjuk Pelaksanaan OMVN 2018</a>
                        </div>
                        <div class="sec-sub-title registration" id="registrasi-online" style="margin-top:0px; display:none">
                            <ol style="margin-top:0px">
                                <b>Syarat Pendaftaran Online :</b>
                                <li>Pendaftaran Online dapat dilakukan <a href="" target="__blank" class="btn btn-success" role="button" style="background-color:#275864; border-color:#275864; margin-bottom:10px">Disini</a> mulai dari tanggal 17 Juli 2017 sampai dengan 31 Oktober 2017.</li>
                                <li>Peserta harus menyiapkan scan/foto kartu pelajar yang masih berlaku.</li>
                                <li>Peserta harus menyiapkan scan pas foto terbaru ukuran 3x4 dengan nama file foto sesuai dengan nama lengkap peserta.</li>
                                <li>4.  Peserta delegasi harus menyiapkan scan/foto surat delegasi dari sekolah. Contoh surat delegasi dapat diunduh  <a href="" target="__blank" class="btn btn-success" role="button" style="background-color:#275864; border-color:#275864; margin-bottom:10px">Disini</a> </li>
                                <li>Peserta harus melunasi biaya pendaftaran pada rekening yang tertera di laman pendaftaran selambat-lambatnya tanggal 5 Oktober 2018 pukul 18.00 WIB</li>
                            </ol>
                            <br>
                            <ol style="margin-top:0px">
                                <b>Alur Pendaftaran Online :</b>
                                <li>Melakukan pendaftaran melalui website OMVN 2018 yaitu www.omvn-um.com</li>
                                <li>Membuat akun peserta dengan mengisikan nama, email, dan nomor handphone yang valid dan aktif.</li>
                                <li>Satu email hanya dapat digunakan untuk membuat satu akun peserta. Peserta yang mendaftar dua kategori sekaligus harus menggunakan email yang berbeda untuk membuat akun</li>
                                <li>Satu email hanya dapat digunakan untuk membuat satu akun peserta.</li>
                                <li>Melakukan pembayaran biaya pendaftaran sesuai dengan kategori yang dipilih melalui <b>BANK BRI a.n. Ditha Ainun Putri (2166-01-002026-53-1)</b></li>
                                <li>Konfirmasi pendaftaran dengan masuk ke akun OMVN dan melampirkan foto bukti pembayaran, serta surat delegasi (bagi peserta delegasi). </li>
                                <li>Panitia akan melakukan verifikasi pembayaran maksimal 1x24 jam. Jika dalam 1x24 pembayaran belum tervalidasi, harap hubungi <b>Web Call Center: Ditha (081234607800)</b></li>
                                <li>Setelah pembayaran dikonfirmasi, isi data sekolah dan data peserta dengan lengkap dan benar. Kunci data dan pastikan tidak ada kesalahan pada data yang telah diisi. </li>
                                <li>Data yang telah dikunci tidak dapat diubah dengan alasan apapun.</li>
                                <li>Panitia tidak bertanggung jawab atas kesalahan peserta dalam melakukan pengisian data.</li>
                                <li>Email yang diisikan pada data peserta dapat digunakan untuk mengikuti Try Out Online. </li>
                                <li>Peserta mencetak kartu bukti pendaftaran (invoice) yang nantinya dapat ditukar dengan kartu peserta pada saat pelaksanaan penyisihan</li>

                            </ol>                           
                        </div>      
                        <div class="sec-sub-title registration" id="registrasi-offline" style="margin-top:0px; display:none">
                            <p class="registration">
                                <ul style="margin-top:0px">
                                    <li>Pendaftaran offline dapat dilakukan mulai tanggal 16 Juli 2018 sampai dengan tanggal 7 Oktober 2018 di tempat pendaftaran masing-masing rayon.</li> 
                                    <li>Mengumpulkan berkas pendaftaran ke tempat pendaftaran rayon 
                                    <p>a.   Formulir pendaftaran,</p>
                                    <p>b.   1 lembar fotokopi kartu pelajar, </p>
                                    <p>c.   1 lembar foto 3x4, </p>
                                    <p>d.   Surat delegasi (bagi peserta delegasi). Contoh surat delegasi dapat diunduh    <a href="https://drive.google.com/file/d/1ySwDJS6eJRWYs-2qxcC2bFyy49RWB-JV/view?usp=sharing" target="__blank" class="btn btn-success" role="button" style="background-color:#275864; border-color:#275864; margin-bottom:10px">Di sini</a>  (surat delegasi di folder File Download)</p>
                                    </li>
                                    <li>Formulir bisa didapatkan melalui sekolah (bagi sekolah yang mendapat surat permohonan delegasi) atau dapat diunduh      <a href="" target="__blank" class="btn btn-success" role="button" style="background-color:#275864; border-color:#275864; margin-bottom:10px">Di sini</a> </li>
                                    <li>Membayar biaya pendaftaran kepada penerima pendaftaran masing-masing rayon.</li>
                                    
                                </ul>   
                            </p>
                            <br>
                            <ol style="margin-top:0px">
                                <b>Alur Pendaftaran Offline</b>
                                <li>Mengisi formulir pendaftaran OMVN 2018 dengan data yang sebenar-benarnya. Nama yang ditulis adalah nama yang akan dicetak pada sertifikat. Gunakan huruf kapital dan bolpoin hitam dalam pengisian formulir.</li>
                                <li>Menyerahkan biaya pendaftaran dan berkas-berkas yang diperlukan ke tempat pendaftaran masing-masing rayon. Adapun contact person pada setiap rayon dapat dilihat pada menu <b>Region.</b></li>
                                    </ul>
                                </li>
                                <li>Setelah menyerahkan biaya pendaftaran dan berkas-berkas yang diperlukan, peserta akan mendapatkan kuitansi pendaftaran.
                                </li>           
                                <li>Panitia akan memberikan akun kepada peserta untuk mengakses Try Out Online dan mencetak bukti pendaftaran (invoice) pada website OMVN 2018 melalui nomor handphone atau email yang dituliskan pada formulir pendaftaran selambat-lambatnya 7x24 jam setelah pendaftaran diterima. (Tidak berlaku untuk pendaftaran OTS)
                                </li>               
                            </ol>
                            <a href="https://drive.google.com/file/d/1uj1yiCa41hWmI8x-pwLfVezA9vb9Gx7h/view?usp=sharing" target="__blank" class="btn btn-success" role="button" style="background-color:#275864; border-color:#275864; margin-top:15px"><i class="fa fa-cloud-download" aria-hidden="true"></i> Formulir Registrasi Offline</a>
                        </div>
                        <div class="sec-sub-title registration" id="fasil-biaya" style="margin-top:0px">
                            <ol style="margin-top:0px">
                                <li>Soal Babak Penyisihan
                                </li>
                                <li>Sertifikat</li>
                                <li>Blocknote</li>
                                <li>Stopmap</li>
                                <li>Bolpoin</li>
                                <li>Id Card</li>
                                <li>Snack dan Softdrink</li>
                                <li>Try Out Online (Bagi yang mendaftar Sebelum 30 September 2018)</li>
                                <br/>
                                <table class="table table-striped table-bordered">
                                    <tr>
                                        <th>Gelombang</th>
                                        <th>Jadwal Pendaftaran</th>
                                        <th>Perorangan*</th>
                                        <th>Beregu*</th>
                                    </tr>
                                    <tr>
                                        <td>Gelombang 1</td>
                                        <td>16 Juli – 19 Agustus 2018</td>
                                        <td>Rp70.000,00</td>
                                        <td>Rp170.000,00</td>
                                    </tr>
                                    <tr>
                                        <td>Gelombang 2</td>
                                        <td>20 Agustus – 5 Oktober 2018</td>
                                        <td>Rp75.000,00</td>
                                        <td>Rp180.000,00</td>
                                    </tr>
                                    <tr>
                                        <td>OTS **</td>
                                        <td>6 – 7 Oktober 2018</td>
                                        <td>Rp100.000,00</td>
                                        <td>Rp200.000,00</td>
                                    </tr>
                                </table>
                                <br/>
                                <li>*Peserta Delegasi mendapat potongan Rp. 5000,00,
                                    **Hanya dibuka untuk pendaftaran offline
                                    </li>

                              
                            </ol>
                            
                        </div>
                        <div class="sec-sub-title registration" id="silabus" style="margin-top:0px; display:none">
                            <a href="https://drive.google.com/open?id=1EqgLHTVyH8HHrRyUXR8AWxZ2unTMmd3i" target="__blank" class="btn btn-success" role="button" style="background-color:#275864; border-color:#275864; margin-bottom:10px"><i class="fa fa-cloud-download" aria-hidden="true"></i> Referensi Buku dan Silabus</a>
                            <a href="https://drive.google.com/file/d/1ORot2touPlt-R0SuxIqOM5tSq7Cqzwav/view?usp=sharing" target="__blank" class="btn btn-success" role="button" style="background-color:#275864; border-color:#275864; margin-bottom:10px"><i class="fa fa-cloud-download" aria-hidden="true"></i> Kumpulan Soal Medspin</a>
                        </div>
                        <div class="sec-sub-title registration" id="hadiah" style="margin-top:0px; display:none">
                            <h3>Perorangan SD, SMP dan SMA</h3>
                            <br/>
                            <div class="col-md-4 col-sm-6 col-xs-12 text-center">
                                <div class="counters-item">
                                    <i class="fa fa-trophy fa-3x"></i>
                                    <strong>1<sup>st</sup></strong>
                                    <p>
                                        Rp2.500.000,00 <br/> Trophy<br/> Sertifikat
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12 text-center">
                                <div class="counters-item">
                                    <i class="fa fa-trophy fa-3x"></i>
                                    <strong>2<sup>nd</sup></strong>
                                    <p>
                                        Rp 1.800.000,00 <br/> Trophy <br/> Sertifikat
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12 text-center">
                                <div class="counters-item">
                                    <i class="fa fa-trophy fa-3x"></i>
                                    <strong>3<sup>rd</sup></strong>
                                    <p>
                                         Rp1.100.000,00 <br/>  Trophy <br/>  Sertifikat
                                    </p>
                                </div>
                            </div>
                            <h3>Beregu SMP dan SMA</h3>
                            <br/>
                            <div class="col-md-4 col-sm-6 col-xs-12 text-center">
                                <div class="counters-item">
                                    <i class="fa fa-trophy fa-3x"></i>
                                    <strong>1<sup>th</sup></strong>
                                    <p>
                                        Rp 4.500.000,00 <br/>Trophy <br/> Sertifikat
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12 text-center">
                                <div class="counters-item">
                                    <i class="fa fa-trophy fa-3x"></i>
                                    <strong>2<sup>nd</sup></strong>
                                    <p>
                                        Rp3.500.000,00 <br/> Trophy <br/> Sertifikat
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12 text-center">
                                <div class="counters-item">
                                    <i class="fa fa-trophy fa-3x"></i>
                                    <strong>3<sup>rd</sup></strong>
                                    <p>
                                        Rp2.500.000,00 <br/> Trophy <br/>  Sertifikat
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12 text-center">
                                <div class="counters-item">
                                    <i class="fa fa-trophy fa-3x"></i>
                                    <strong><sup></sup></strong>
                                    <br>
                                    <p>
                                        Peringkat 4-15 SD <br> Trophy <br> Sertifikat
                                    </p>
                                </div>
                            </div>                          
                            <div class="col-md-4 col-sm-6 col-xs-12 text-center">
                                <div class="counters-item">
                                    <i class="fa fa-trophy fa-3x"></i>
                                    <strong><sup></sup></strong>
                                    <p>
                                        Peringkat 4, 5 SMP dan SMA kategori perorangan  <br>Trophy <br> Sertifikat
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12 text-center">
                                <div class="counters-item">
                                    <i class="fa fa-trophy fa-3x"></i>
                                    <strong><sup></sup></strong>
                                    <p>
                                       Juara Umum SD, SMP dan SMA <br> Trophy Bergilir 
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12 text-center">
                                <div class="counters-item">
                                    <i class="fa fa-trophy fa-3x"></i>
                                    <strong><sup></sup></strong>
                                    <p>
                                       Juara 1, 2 dan 3 SMA kategori perorangan berhak <br> mendapatkan  rekomendasi masuk <br> Jurusan Matematika Universitas Negeri Malang
                                    </p>
                                </div>
                            </div>                                                                          
                        </div>
                    </div>
                </div>
                
            </div>
        
        </section>

        <section id="jadwal-perlombaan" class="features cd-horizontal-timeline">
            <div class="sec-title text-center mb50 wow bounceInDown animated" data-wow-duration="500ms">
                <h2>Jadwal Perlombaan</h2>
                <div class="devider"><i class="fa fa-calendar fa-lg"></i></div>
            </div>
            <div class="hidden-xs timeline">
                <div class="events-wrapper">
                    <div class="events">
                        <ol>
                            <li><a href="#0" data-date="01/01/2017" class="selected"><strong>Pendaftaran <br> Gelombang 1</strong></a></li>
                            <li><a href="#0" data-date="01/04/2017" ><strong>Pendaftaran <br> Gelombang 2</strong></a></li>                          
                            <li><a href="#0" data-date="01/07/2017" ><strong>Try Out <br> Online 1</strong></a></li>                                                        
                            <li><a href="#0" data-date="01/09/2017" ><strong>Try Out <br> Online 2</strong></a></li>                                                                                    
                            <li><a href="#0" data-date="01/12/2017" ><strong>Pendaftaran OTS </strong></a></li>                                                                                                                
                            <li><a href="#0" data-date="01/02/2018"><strong>Babak Penyisihan</strong></a></li>                                                                                                              
                            <li><a href="#0" data-date="30/05/2018"><strong>Babak Semifinal</strong></a></li>
                            <li><a href="#0" data-date="30/08/2018"><strong>Babak Final </strong></a></li>
                        </ol>
                        <span class="filling-line" aria-hidden="true"></span>
                    </div> <!-- .events -->
                </div> <!-- .events-wrapper -->
                    
                <ul class="cd-timeline-navigation" style="list-style: none">
                    <li><a href="#0" class="prev inactive">Prev</a></li>
                    <li><a href="#0" class="next">Next</a></li>
                </ul> <!-- .cd-timeline-navigation -->
            </div> <!-- .timeline -->

            <div class="hidden-xs events-content">
                <ol>
                    <li class="selected" data-date="01/01/2017">
                        <div class="service-desc">
                            <h3 style="margin-bottom: 10px">16 Juli – 19 Agustus 2018</h3>
                            <p style="margin-bottom: 10px"><strong>Pendftaran gelombang  1 </strong></p>
                            <p>Merupakan masa pendaftaran awal dengan harga yang lebih terjangkau, pendaftaran dilakukan secara online di website omvn-um.com dan offline di masing-masing rayon seluruh Indonesia.</p>
                        </div>                      
                    </li>
                    <li data-date="01/04/2017">
                        <div class="service-desc">
                            <h3 style="margin-bottom: 10px">20 Agustus – 5 Oktober 2018</h3>
                            <p style="margin-bottom: 10px"><strong>Pendaftaran gelombang  2 <strong></p>
                            <p>Merupakan masa pendaftaran kedua dengan biaya lebih tinggi dari gelombang 1. Pendaftaran dilakukan secara online di website omvn-um.com dan offline di masing-masing rayon seluruh Indonesia</p>
                            
                        </div>                      
                    </li>                   
                    <li data-date="01/07/2017">
                        <div class="service-desc">
                            <h3 style="margin-bottom: 10px">26 Agustus 2018</h3>
                            <p style="margin-bottom: 10px"><strong>Try Out Online 1</strong></p>
                            <p>Merupakan fasilitas yang disediakan untuk melatih kemampuan peserta OMVN 2018 dalam menghadapi babak penyisihan. Try Out Online 1 dapat diikuti oleh peserta yang telah terdaftar sebelum tanggal 26 Agustus 2018. Peserta tidak wajib mengikuti Try Out. Nilai dalam Try Out tidak mempengaruhi penilaian babak penyisihan.</p>
                        </div>                      
                    </li>                   
                    <li data-date="01/09/2017">
                        <div class="service-desc">
                            <h3 style="margin-bottom: 10px">30 September  2018</h3>
                            <p style="margin-bottom: 10px"><strong>Try Out Online 2</strong></p>
                            <p>Merupakan fasilitas yang disediakan untuk melatih kemampuan peserta OMVN 2018 dalam menghadapi babak penyisihan. Try Out Online 2 dapat diikuti oleh peserta yang telah terdaftar sebelum tanggal 30 September 2018. Peserta tidak wajib mengikuti Try Out. Nilai dalam Try Out tidak mempengaruhi penilaian babak penyisihan.</p>
                        </div>                      
                    </li>                                                           
                    <li data-date="01/12/2017">
                        <div class="service-desc">
                            <h3 style="margin-bottom: 10px">6 – 7 Oktober 2018</h3>
                            <p style="margin-bottom: 10px"><strong>Pendaftaran OTS </strong></p>
                            <p>Merupakan masa pendaftaran akhir yang dapat dilakukan pada H-1 dan hari H babak penyisihan. Pendaftaran ini hanya dibuka pendaftaran offline di tempat pelaksanaan babak penyisihan.</p>
                        </div>                      
                    </li>                                                                               
                    <li data-date="01/02/2018">
                        <div class="service-desc">
                            <h3 style="margin-bottom: 10px">7 Oktober 2018 (di masing-masing rayon)</h3>
                            <p style="margin-bottom: 10px"><strong>Babak Penyisihan</strong></p>
                            <p>Merupakan babak pertama OMVN yang dilakukan dengan tes tulis secara individu di masing-masing rayon seluruh Indonesia. </p>
                        </div>                      
                    </li>                           
                    <li data-date="30/05/2018">
                        <div class="service-desc">
                            <h3 style="margin-bottom: 10px">20 Oktober 2018 (Gedung O7 Matematika FMIPA Universitas Negeri Malang)</h3>
                            <p style="margin-bottom: 10px"><strong>Babak Semifinal</strong></p>
                            <p>Merupakan babak kedua untuk peserta OMVN yang lolos babak penyisihan. Pada babak ini diikuti oleh peserta SMP dan SMA. Untuk peserta SD langsung mengikuti Babak Final 1 dan 2 pada hari yang sama. Babak ini dilaksanakan di gedung O7 Matematika Universitas Negeri Malang</p>         
                            <br>                        
                            
                        </div>                      
                    </li>                                                       
                    <li data-date="30/08/2018">
                        <div class="service-desc">
                            <h3 style="margin-bottom: 10px">21 Oktober 2018 (Gedung O7 Matematika FMIPA Universitas Negeri Malang)</h3>
                            <p style="margin-bottom: 10px"><strong>Babak Final</strong> </p>
                            <p>Merupakan babak terakhir penentuan juara OMVN 2018  yang akan dilaksanakan di gedung O7 Matematika Universitas Negeri Malang. Tiap jenjang memiliki metode pelaksanaan masing-masing (dapat dilihat di guidebook).</p>
                            <br>
                            
                        </div>                      
                    </li>                                                                                   
                </ol>
            </div> <!-- .events-content -->
            <div class="visible-xs">
                <div class="container">
                    <div class="row">                       
                        <div class="col-md-4 wow fadeInUp" data-wow-duration="500ms">
                            <div class="service-item">
                                <div class="service-icon">
                                    <i class="fa fa-pencil fa-2x"></i>
                                </div>
                                
                                <div class="service-desc">
                                    <h3 style="margin-bottom: 10px">Early Registration</h3>
                                    <p style="margin-bottom: 10px"><strong>17 Juli - 31 Juli 2017, Online</strong></p>
                                    <p>Merupakan masa pendaftaran dengan biaya lebih murah yang hanya dilakukan <i>online</i> di <i>website</i> resmi Medspin 2017.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 wow fadeInUp" data-wow-duration="500ms">
                            <div class="service-item">
                                <div class="service-icon">
                                    <i class="fa fa-send fa-2x"></i>
                                </div>
                                
                                <div class="service-desc">
                                    <h3 style="margin-bottom: 10px">Reguler</h3>
                                    <p style="margin-bottom: 10px"><strong>1 Agustus - 31 Oktober 2017, Panwil Kota Masing-Masing & Online</strong></p>
                                    <p>Merupakan masa pendaftaran dengan biaya normal yang dilakukan secara Online di website resmi Medspin 2017 dan secara Offline pada Panitia Wilayah (Panwil) masing-masing.</p>
                                    <p>Pendaftaran Offline di Kampus A FK UNAIR Surabaya dilakukan sesuai jadwal berikut: Senin—Kamis pukul 16.00—18.00 WIB; Jumat pukul 15.00—17.00 WIB; dan Sabtu pukul 09.00—13.00 WIB.</p>
                                </div>
                            </div>
                        </div>          
                        <div class="col-md-4 wow fadeInUp" data-wow-duration="500ms">
                            <div class="service-item">
                                <div class="service-icon">
                                    <i class="fa fa-pencil fa-2x"></i>
                                </div>
                                
                                <div class="service-desc">
                                    <h3 style="margin-bottom: 10px">Try Out 1 <i>(tidak diwajibkan)</i></h3>
                                    <p style="margin-bottom: 10px"><strong>8 Oktober 2017, Online</strong></p>
                                    <p>Ajang berlatih pertama yang disediakan gratis oleh Medspin 2017 untuk mengerjakan soal babak penyisihan bagi peserta Medspin 2017 (Nilai Try Out tidak berpengaruh terhadap penilaian Babak Penyisihan).</p>
                                </div>
                            </div>
                        </div>      
                        <div class="col-md-4 wow fadeInUp" data-wow-duration="500ms">
                            <div class="service-item">
                                <div class="service-icon">
                                    <i class="fa fa-send fa-2x"></i>
                                </div>
                                
                                <div class="service-desc">
                                    <h3 style="margin-bottom: 10px">Try Out 2 <i>(tidak diwajibkan)</i></h3>
                                    <p style="margin-bottom: 10px"><strong>22 Oktober 2017, Online</strong></p>
                                    <p>Ajang berlatih kedua dan terakhir yang disediakan gratis oleh Medspin 2017 untuk mengerjakan soal Babak Penyisihan bagi peserta Medspin 2017 (Nilai Try Out tidak berpengaruh terhadap penilaian Babak Penyisihan).</p>
                                </div>
                            </div>
                        </div>      
                        <div class="col-md-4 wow fadeInUp" data-wow-duration="500ms">
                            <div class="service-item">
                                <div class="service-icon">
                                    <i class="fa fa-times fa-2x"></i>
                                </div>
                                
                                <div class="service-desc">
                                    <h3 style="margin-bottom: 10px">Penutupan Pendaftaran</h3>
                                    <p style="margin-bottom: 10px"><strong>31 Oktober 2017</strong></p>
                                    <p>Penutupan kesempatan berpartisipasi dalam ajang Medspin 2017.</p>
                                </div>
                            </div>
                        </div>      
                        <div class="col-md-4 wow fadeInUp" data-wow-duration="500ms">
                            <div class="service-item">
                                <div class="service-icon">
                                    <i class="fa fa-pencil fa-2x"></i>
                                </div>
                                
                                <div class="service-desc">
                                    <h3 style="margin-bottom: 10px">Babak Penyisihan</h3>
                                    <p style="margin-bottom: 10px"><strong>5 November 2017, Panwil Kota Masing-Masing & Online</strong></p>
                                    <p>Babak pertama perlombaan Medspin 2017. Menguji kemampuan peserta dengan soal pilihan ganda yang dilaksanakan dengan Ujian Tulis tertutup pada lokasi-lokasi yang disediakan Medspin 2017 dan Ujian <i>Online</i> di modul ujian <i>website</i> Medspin 2017 yang dilakukan serentak pada waktu yang sudah ditentukan. Penentuan cara ujian dilakukan peserta Medspin 2017 sejak masa pendaftaran. </p>
                                </div>
                            </div>
                        </div>                                                                                  
                        <div class="col-md-4 wow fadeInUp" data-wow-duration="1500ms">
                            <div class="service-item">
                                <div class="service-icon">
                                    <i class="fa fa-paper-plane fa-2x"></i>
                                </div>
                                
                                <div class="service-desc">
                                    <h3 style="margin-bottom: 10px">Babak Perempat Final</h3>
                                    <p style="margin-bottom: 10px"><strong>18 November 2017, FK UNAIR Surabaya</strong> </p>
                                    <p>Babak kedua perlombaan Medspin 2017. Peserta unjuk kebolehan dalam Essay Rally dan berusaha memecahkan Teka-teki Kedokteran di lingkungan Fakultas Kedokteran Universitas Airlangga. Acara ini berlangsung dari pukul 06.30—11.00 WIB, serangkai dengan babak SemiFinal.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row" style="margin-top:20px">

                        <div class="col-md-4 wow fadeInUp" data-wow-duration="2000ms">
                            <div class="service-item">
                                <div class="service-icon">
                                    <i class="fa fa-pencil fa-2x"></i>
                                </div>
                                
                                <div class="service-desc">
                                    <h3 style="margin-bottom: 10px">Babak Semifinal</h3>
                                    <p style="margin-bottom: 10px"><strong>18 November 2017, FK UNAIR Surabaya</strong> </p>
                                    <p>Babak ketiga perlombaan Medspin 2017. Peserta kembali diuji kemampuannya dengan soal teori essay biologi, fisika, dan kimia yang berkaitan dengan bidang kedokteran yang dilaksanakan di Fakultas Kedokteran Universitas Airlangga. Acara ini berlangsung dari pukul 06.30—11.00 WIB, serangkai dengan Babak Perempat Final.</p>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 wow fadeInUp" data-wow-duration="2500ms">
                            <div class="service-item">
                                <div class="service-icon">
                                    <i class="fa fa-flask fa-2x"></i>
                                </div>
                                
                                <div class="service-desc">
                                    <h3 style="margin-bottom: 10px">Babak Final</h3>
                                    <p style="margin-bottom: 10px"><strong>19 November 2017, FK UNAIR Surabaya</strong> </p>
                                    <p>Babak keempat perlombaan Medspin 2017. Peserta menghadapi uji praktikum biologi, fisika, kimia, dan kedokteran aplikatif serta teori essay panjang biologi, fisika, dan kimia di lingkungan Fakultas Kedokteran Universitas Airlangga. Acara ini berlangsung dari pukul 06.30—11.00 WIB.</p>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 wow fadeInUp" data-wow-duration="3000ms">
                            <div class="service-item">
                                <div class="service-icon">
                                    <i class="fa fa-slideshare fa-2x"></i>
                                </div>
                                
                                <div class="service-desc">
                                    <h3 style="margin-bottom: 10px">Babak Grand Final</h3>
                                    <p style="margin-bottom: 10px"><strong>19 November 2017, FK UNAIR Surabaya</strong> </p>
                                    <p>Babak terakhir perlombaan Medspin 2017. Penentuan pemenang lomba Medspin 2017 melalui presentasi hasil studi kasus, dan lomba cepat tepat di Fakultas Kedokteran Universitas Airlangga. Acara ini berlangsung dari pukul 11.00—16.00 WIB.</p>
                                </div>
                            </div>
                        </div>
        
                    </div>
                </div>
            </div>
        </section>
        
        
       

 <!--  <script src="assets/theme/js/script.js"></script> -->
		
		
		<!-- <section id="twibbon" class="team">
			<div class="container">
				<div class="row">		
					<div class="sec-title text-center wow fadeInUp animated" data-wow-duration="700ms">
						<h2>Twibbon OMVN 2018</h2>
						<div class="devider"><i class="fa fa-heart-o fa-lg"></i></div>
					</div>

					<div class="sec-sub-title text-center">
						<p>
							Ayo ajak teman-teman kalian berkompetisi dalam OMVN 2018 dan upload poster dibawah ini 
						</p>
					</div>					

					<div class="col-md-4 wow fadeInRight animated" data-wow-duration="500ms">
						<img src="img/twibbon.jpg" alt="" class="img-responsive img-portfolio" style="margin-bottom:20px; margin-top:35px">
						<a class="btn btn-primary pull-right" href="img/twibbon.jpg" download="twibbon-image">Download Foto Twibbon</a>
					</div>
					
					<div class="col-md-8">
						<div class="sec-sub-title wow fadeInRight animated" data-wow-duration="2000ms">
							<p style="text-align : justify">
								DID YOU KNOW 
							</p>
							<br/>
							<p style="text-align : justify">
								Being part of Asean Economic Community (AEC), Indonesia also loaded with sea of opportunities for the people to expand their bussiness and be more successful at. Unfortunately, a research conducted to students Indonesia found that only 53 of 100 pupils are ready for competition which soon they encounter, especially in AEC era.
							</p>
							<p style="text-algin : justify">
								A famous man, Thomas Edison, known for his lightbulb invention after 1000 of unsuccessful trials once says "Opportunity is missed by most people because it is dressed in overalls and looks like hardwork"
							</p>	
							<p style="text-align : justify">
								Therefore, 
							</p>
							<p style="text-align : justify">
								I, (name), from (school) am taking my opportunity and ready to compete with thousands of student from Indonesia and ASEAN countries in the biggest medical competition in Indonesia, OMVN 2018! 							
							</p>	
							<br/>
							<p style="text-algin : justify">
								-I am part of the 53, are you?
							</p>
							<p style="text-algin : justify">
								-I am ready, are you?							
							</p>							
							<br/>
							<p style="text-algin : justify">
								#BeAMedicalHero
							</p>							
							<p style="text-algin : justify">
								#Medspin2017
							</p>							
							<p style="text-algin : justify">
								#FKUnair
							</p>		
							<br/>																			
							<p style="text-algin : justify">
								Tag 5 of your friends in your social media (Instagram, Facebook, Line, etc)
							</p>														
							<p style="text-algin : justify">
								-to let them know?
							</p>														
							<p style="text-algin : justify">
								-to share cause you care 💕
							</p>																												
						</div>
					</div>
				</div>
			</div>
		</section>
 -->
		

		<section id="galeri" class="works clearfix">
			<div class="container">
				<div class="row">
					<div class="sec-title text-center">
						<h2>Galeri OMVN 2018</h2>
						<div class="devider"><i class="fa fa-heart-o fa-lg"></i></div>
					</div>
				</div>
			</div>
			<div class="project-wrapper" style="margin-top:50px">
				@for($i=1; $i<9; $i++)
				<figure class="mix work-item branding"><img src="assets/img/omvn-{{$i}}.jpg">
					<figcaption class="overlay"><a class="fancybox" href="img/medspin-{{$i}}.jpg"><i class="fa fa-eye fa-lg"></i></a>
					</figcaption>
				</figure>
				@endfor
			</div>
            <!--TEASER-->
			<!-- <iframe class="video-teaser" frameborder="0"  src="https://www.youtube.com/embed/hCafnb8-n8M" allowfullscreen></iframe> -->
		</section>

		<!-- <section id="sponsors" class="contact">
			<div class="container">
				<hr>
				<div class="row mb50">														
					<h3 style="text-align: center">For Sponsor Inquires Please Contact :</h3>
					<br>
					<div align="center" class="col-lg-6 col-md-6 col-sm-7 col-xs-12 wow fadeInDown animated" data-wow-duration="500ms" data-wow-delay="300ms">
						<div class="contact-address">
							<h4 style="margin-top:10px">Ayu Rahmanita Putri</h4>
							<p>Email : ayu7485@live.com atau putriayu040998@gmail.com</p>
							<p>WhatasApp : 085649729709</p>
						</div>
					</div>
					<div align="center" class="col-lg-6 col-md-6 col-sm-7 col-xs-12 wow fadeInDown animated" data-wow-duration="500ms" data-wow-delay="300ms">
						<div class="contact-address">
							<h4 style="margin-top:10px">Eka Candra Setiawan</h4>
							<p>Email : ekacandrasetyawan@gmail.com</p>
							<p>WhatasApp : 081252324137</p>							
						</div>
					</div>															
				</div>
				<hr>
			</div>			
		</section>		 -->
        <br/><br/>
        <section id="tryout" class="features">
            <div class="container">
                <div class="row">
                
                    <div class="sec-title text-center mb50 wow bounceInDown animated" data-wow-duration="500ms">
                        <h2>Jadwal Tryout</h2>
                        
                        
                        
                    </div>

                    <br/>
                    <p>Try Out Online dapat diakses oleh peserta yang telah terdaftar sebelum tanggal 30 September 2018. Jadwal Try Out Online adalah sebagai berikut:</p>
                    <table class="table table-striped table-bordered">
                        <tr>
                            <th>Agenda</th>
                            <th>Hari dan Tanggal Pelaksanaan</th>
                            <th>Waktu Pelaksanaan</th>
                        </tr>
                        <tr>
                            <td>Try Out Online I </td>
                            <td>26 Agustus 2018 </td>
                            <td>07.00 – 22.00 WIB </td>
                        </tr>
                        <tr>
                            <td>Try Out Online II </td>
                            <td>30 September 2018 </td>
                            <td>07.00 – 22.00 WIB </td>
                        </tr>
                       
                    </table>
                    
    
                </div>
            </div>
        </section>
        <br/><br/>
		
		
        <br/><br/>
                
		<section id="tanya-medspin" class="contact">
			<div class="container">
				<div class="row mb50">
				
					<div class="sec-title text-center mb50 wow fadeInDown animated" data-wow-duration="500ms">
						<h2>Tanya OMVN</h2>
						<div class="devider"><i class="fa fa-question fa-lg"></i></div>
					</div>
					
					<div class="sec-sub-title text-center wow rubberBand animated" data-wow-duration="1000ms">
						<p>Ada pepatah, tak kenal maka tak sayang. Untuk itu, ayo berkenalan dengan kami sambil bertanya tentang OMVN 2018 melalui segala kanal yang tersedia di bawah ini</p>
					</div>
					
					<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 wow fadeInLeft animated" data-wow-duration="500ms">
						<div class="contact-address">
							<h3 style="margin-bottom:10px">Sekretariat OMVN</h3>
							<p>Gedung SPA O8 Lantai 1 Fakultas Matematika dan Ilmu Pengetahuan Alam Universitas Negeri Malang</p>
							<p>Jalan Semarang  5 Malang 65145</p>
							<p>Contact Person :</p>
							<!-- <p>Qonita (+6281230391660)</p>
							<p>Devina (+6281252930000)</p> -->
                            <p>Tiara  (085249580904)</p>

                            <p>Neny   081232254863 (SMA)</p>
                            <p>Ilmi   083832763647 (SMP)</p>
                            <p>Rianti 082299131176 (SD)</p>
							<p>Line : @oin5513w</p>
							<p>Email :</p>
                            <p>Instagram: omvn_um</p>
                            <p>Facebook: Olimpiade Matematika “Vektor” Nasional</p>
						</div>
					</div>
					
					<div class="col-lg-8 col-md-8 col-sm-7 col-xs-12 wow fadeInDown animated" data-wow-duration="500ms" data-wow-delay="300ms">
						<div id="disqus_thread"></div>
						<script>
						var disqus_config = function ()  {
                        this.page.url = www.omvn-id.com;  // Replace PAGE_URL with your page's canonical URL variable
                        this.page.identifier = 'omvn-id'; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                        };

                        (function() { // DON'T EDIT BELOW THIS LINE
                        var d = document, s = d.createElement('script');
                        s.src = 'https://omvn-2018.disqus.com/embed.js';
                        s.setAttribute('data-timestamp', +new Date());
                        (d.head || d.body).appendChild(s);
                        })();
                        </script>
                        <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
					</div>
					
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-12 wow fadeInRight animated" data-wow-duration="500ms" data-wow-delay="600ms">
						<ul class="footer-social">
							<li><a href="https://web.facebook.com/Medspin-FK-Unair-2017-669727393093938/?fref=ts"><i class="fa fa-facebook fa-2x"></i></a></li>
							<li><a href="https://www.instagram.com/medspinfkunair/?hl=id"><i class="fa fa-instagram fa-2x"></i></a></li>
							<li><a href="https://www.youtube.com/channel/UClhaRq4HolNot70zUxGr0Bg"><i class="fa fa-youtube fa-2x"></i></a></li>
						</ul>
					</div>
					
				</div>
			</div>
			
		<div class="google-map"><iframe frameborder="0" style="border:0;width:100%" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyA0Dx_boXQiwvdz8sJHoYeZNVTdoWONYkU&amp;q=place_id:ChIJOYjQvYGCeC4RH4P6v2jyFck" allowfullscreen=""></iframe></div>
			
		</section>
		
		
		<footer id="footer" class="footer" style="padding: 10px 0;">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<p class="copyright text-center">
							Copyright © OMVN 2018. Powered by <a href="https://www.olimpiade.id/">Olimpiade.id</a>
						</p>
					</div>
				</div>
			</div>
		</footer>
		
		<a href="javascript:void(0);" id="back-top"><i class="fa fa-angle-up fa-3x"></i></a>

        <script src="js/jquery-1.11.1.min.js"></script>
        <script src="js/jquery.singlePageNav.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.fancybox.pack.js"></script>
        <script src="js/jquery.mixitup.min.js"></script>
        <script src="js/jquery.parallax-1.1.3.js"></script>
        <script src="js/jquery-countTo.js"></script>
        <script src="js/jquery.appear.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/3.32/jquery.form.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.11.1/jquery.validate.min.js"></script>
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
        <script src="js/jquery.easing.min.js"></script>
        <script src="js/wow.min.js"></script>
		<script src="js/jquery.mobile.custom.min.js"></script>
		<script src="js/main.js"></script> <!-- Resource jQuery -->		
        <script src="js/jQuery.verticalCarousel.js"></script>
		<script src="js/flipclock.js"></script>		
		<script src="js/jquery.countdown.js"></script>	


		<script type="text/javascript">
			$(document).ready(function(){
				$("#newsModal").modal('show');
			});
		</script>
		<!-- News Modal -->
		<div id="newsModal" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Pengumuman Terbaru</h4>
					</div>
					<div class="modal-body">
					<p>
						Hello Glory of Numbers! 
						<br/>
						Selamat yaa bagi kalian yang lolos ke quarterfinal. Dan bagi kalian yang belum lolos, jangan berkecil hati! Tetap semangat dan ikutilah lomba-lomba yang lain
						<br/>
						Berikut adalah file-file yang harus didownload bagi tim yang lolos ke quarterfinal:
						<br/>	
						<ul style="margin-left: 20px">
							<li>
								<a target="__blank" href=" https://drive.google.com/file/d/1uj1yiCa41hWmI8x-pwLfVezA9vb9Gx7h/view?usp=sharing">Formulir Registrasi</a>
							</li>
							<li>
								<a target="__blank" href=" https://drive.google.com/file/d/1TVWp8YoRDopUUVH_EEs9drv-Ijd6m8jk/view?usp=sharing">Guide Book</a>
							</li>
							<!-- <li>
								<a target="__blank" href="https://drive.google.com/open?id=1504Ju9iX4UNd028nYOiXCoHdip3fgq-s">Pengumuman 150 besar</a>
							</li> -->
						</ul>
					</p>
					<br>
					<p>
						#TheGloryofNumbers
						#OMVN2017
												
					</p>					
					</div>
					<div class="modal-footer">					
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
			

        <script>
            $(".verticalCarousel").verticalCarousel({
                currentItem: 1,
                showItems: 2,
            });	
        </script><script type="text/javascript">
		<script>
			var wow = new WOW ({
				boxClass:     'wow',      
				animateClass: 'animated', 
				offset:       120,          
				mobile:       false,      
				live:         true        
			  }
			);	
			wow.init();
		</script> 
        <script src="js/custom.js"></script>
		
		<script type="text/javascript">			
			$(document).ready(function() {
				var clock;
				var date  = new Date('10/31/2017 5:00:01 PM UTC');
				var now   = new Date();
				// window.alert(date);
				var diff  = date.getTime()/1000 - now.getTime()/1000;

				clock = $('.clock').FlipClock(diff, {
					clockFace: 'DailyCounter',
					autoStart: false,
					callbacks: {
						stop: function() {
							$('.message').html('The clock has stopped!')
						}
					}
				});						
				clock.setCountdown(true);
				clock.start();
			});
		</script>
		<script>
			$('#clock').countdown('2017/11/01', function(event) {
			$(this).html(event.strftime('%D days %H:%M:%S'));
			});		
		</script>

		@foreach($regions as $i => $region)
		<div id="region-{{$i}}" class="modal fade" role="dialog">
			<div class="modal-dialog">

				<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Lokasi Penyisihan Panwil {{$region->city}}</h4>
				</div>
				<div class="modal-body">
					<p> <strong><h4>{{$region->tempat}}</h4></strong> </p>
					<br>
					<p><h4>Kontak : {{$region->pic}}</h4></p>
                    <br>
                    <p><h4>Pendaftaran Offline : {{$region->daftar}}</h4></p>   					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
				</div>

			</div>
		</div>
		@endforeach
    </body>
</html>
