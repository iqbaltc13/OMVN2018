<!DOCTYPE html>
<html lang="id">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>MEDSPIN 2017 - Be a Medical Hero!</title>		
        <meta name="description" content="Medical Science and Application (MEDSPIN) 2017 adalah salah satu lomba terbesar yang diadakan oleh Fakultas Kedokteran Universitas Airlangga. Lomba ini akan diadakan bagi para pelajar tingkat SMA / sederajat. Lomba ini bersifat invitasi dan terbuka bagi seluruh pelajar SMA/sederajat dari semua kelas (X, XI, dan XII) baik SMA negeri maupun swasta nasional maupun internasional.">
        <meta name="keywords" content="lomba, medspin, kimia, fisika, biologi, matematika, unair, universitas airlangga, kompetisi, kedokteran">
        <meta name="author" content="Fakultas Kedokteran Universitas Airlangga">
        <meta name="viewport" content="width=device-width, initial-scale=1">

		<link rel="icon" type="image/png" href="img/logo.png">
				
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>

        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/jquery.fancybox.css">
        <link rel="stylesheet" href="css/animate.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/media-queries.css">
		<link rel="stylesheet" href="css/style-timeline.css">
        <link rel="stylesheet" href="css/style-list.css">
        <link rel="stylesheet" href="css/jQuery.verticalCarousel.css">		
		<link rel="stylesheet" href="css/flipclock.css">
		<link rel="stylesheet" href="css/font-awesome.css">		
        <script src="js/modernizr-2.6.2.min.js"></script>

    </head>
<body id="body">
	<div id="preloader">
			<img src="img/preloader.gif" alt="Preloader" class="heavy-rotation">
		</div>

<section id="pengumuman" class="team">
	<div class="container">
		<div class="row">		
			<div class="sec-title text-center wow fadeInUp animated" data-wow-duration="700ms">
				<h2>Pengumuman Tryout 2</h2>
				<div class="devider"><i class="fa fa-heart-o fa-lg"></i></div>
				<div class="col-md-12">
					<div class="sec-sub-title wow fadeInRight animated" data-wow-duration="2000ms">
						<p style="margin-bottom: 2%">Berikut ini adalah daftar peserta yang menjadi 20 terbaik pada tryout 2 MEDSPIN 2017!</p>
						<p style="margin-bottom: 2%">Untuk pembahasan soal tryout 1 dan 2 bisa kamu download pada tombol dibawah ini</p>
						<a href="https://drive.google.com/file/d/0B1WOK5SykYHFUnlhRTZJckJKVWM/view?usp=drivesdk" class="btn btn-default" style="margin-bottom: 1%" target="_blank">Pembahasan Tryout 1 MEDSPIN 2017</a>
						<a href="https://drive.google.com/file/d/0B2TftvvMGTbfbmJRNkRubVlkQ28/view?usp=sharing" class="btn btn-default" style="margin-bottom: 1%" target="_blank">Pembahasan Tryout 2 MEDSPIN 2017</a>
						<div class="table-responsive">
							<table class="table" style="margin: 0 auto; width: 35%">
								<thead>
									<th style="text-align: center">No</th>
									<th style="text-align: center">No_peserta</th>
								</thead>
								@php ($no = 1)
								@foreach($result as $peserta)
								<tr>
									<td>{{$no}}</td>
									<td>{{$peserta->no_peserta == null ? 'Pengisisan Data Belum Lengkap' : $peserta->no_peserta}}</td>
								</tr>
								@php ($no++)
								@endforeach
							</table>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
	

</section>

<footer id="footer" class="footer" style="padding: 10px 0;">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<p class="copyright text-center">
							Copyright © OMVN 2018. Powered by <a href="https://www.olimpiade.id/">Olimpiade.id</a>
						</p>
					</div>
				</div>
			</div>
		</footer>
		
		<a href="javascript:void(0);" id="back-top"><i class="fa fa-angle-up fa-3x"></i></a>

        <script src="js/jquery-1.11.1.min.js"></script>
        <!-- <script src="js/jquery.singlePageNav.js"></script> -->
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.fancybox.pack.js"></script>
        <script src="js/jquery.mixitup.min.js"></script>
        <script src="js/jquery.parallax-1.1.3.js"></script>
        <script src="js/jquery-countTo.js"></script>
        <script src="js/jquery.appear.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/3.32/jquery.form.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.11.1/jquery.validate.min.js"></script>
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
        <script src="js/jquery.easing.min.js"></script>
        <script src="js/wow.min.js"></script>
		<script src="js/jquery.mobile.custom.min.js"></script>
		<script src="js/main.js"></script> <!-- Resource jQuery -->		
        <script src="js/jQuery.verticalCarousel.js"></script>
		<script src="js/flipclock.js"></script>		
		<script src="js/jquery.countdown.js"></script>
		<script src="js/custom.js"></script>
</body>
</html>