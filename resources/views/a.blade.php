<section id="news-countdown" class="features">
			<div class="container">
				<div class="row">
					
					<div class="col-md-5">
						<div class="verticalCarousel">
							<div class="verticalCarouselHeader">
								<h3>Pengumuman Terbaru</h3>
								<a href="#" class="vc_goDown"><i class="fa fa-fw fa-angle-down"></i></a>
								<a href="#" class="vc_goUp"><i class="fa fa-fw fa-angle-up"></i></a>
							</div>
							<ul class="verticalCarouselGroup vc_list">
								@foreach($news as $new)
								<li>
									<h4>{!!$new->title!!}</h4>
									<p>{!!$new->content!!}</p>
								</li>
								@endforeach
							</ul>
						</div>						
					</div>
				</div>
			</div>				
		</section>		 
        <!-- @include('partials.about') -->